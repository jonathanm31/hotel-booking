<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Habitacion;
use App\Traits\HabitacionTrait;
use Mockery\Exception;

class HabitacionController extends Controller
{
    protected $crud;
    public function __construct()
    {
        $this->crud = new HabitacionTrait();
        $this->middleware('auth');
    }
    public function habitaciones(Request $request){
        $habitacionesJson = $this->crud->habitaciones($request['pagina']);
        return view('habitaciones.habitaciones')->with('habitaciones',json_decode($habitacionesJson));
    }
    public function habitacion(Request $request){
        $habitacionJson = $this->crud->habitacion($request['id']);
        return view('habitaciones.habitacion')->with('habitacion',json_decode($habitacionJson));
    }
    public function nuevaHabitacion(Request $request){
       $respuesta = $this->crud->nuevaHabitacion($request);
       return $respuesta;
    }
    public function editarHabitacion(Request $request){
        $respuesta = $this->crud->editarHabitacion($request);
        return $respuesta;
    }
    public function eliminarImagen(Request $request){
        $idhabitacion = $request['idhabitacion'];
        $imagen = $request['imagen'];
        return $this->crud->eliminarImagen(["idhabitacion" => $idhabitacion , "imagen" => $imagen ]);
    }
    public function agregarImagen(Request $request){
        $idhabitacion = $request['idhabitacion'];
        $imagen = $request->file('imagen');
        return $this->crud->eliminarImagen(["idhabitacion" => $idhabitacion , "imagen" => $imagen ]);
    }
    public function eliminarHabitacion(Request $request){
        $respuesta = $this->crud->eliminarHabitacion($request);
        return $respuesta;
    }
    public function liberarHabitacion(Request $request){
        $respuesta = $this->crud->libre($request);
        return $respuesta;
    }

}
