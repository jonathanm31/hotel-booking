<?php

namespace App\Http\Controllers;

use App\Traits\ArticulosTrait;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
class InventarioController extends Controller
{

    private $articuloCrud;
    public function __construct()
    {
        $this->middleware('auth');
        $this->articuloCrud = new ArticulosTrait();
    }

    public function inventario(){
        $articulos = $this->articuloCrud->articulos_sp();
        $inventarios = $this->articuloCrud->articulosInventario();
        return View('inventario.inventario')->with('articulos',json_decode($articulos))->with('inventario',json_decode($inventarios));
    }
    public function moviemiento(Request $request){
        $productos = json_decode($request['productos']);

        foreach ($productos as $producto){
            $data = ["idarticulo" => $producto->idproducto,"cantidad" => $producto->cantidad];
            if($producto->tipo > 0){
                $this->articuloCrud->entrada($data);
            }else{
                $this->articuloCrud->salida($data);
            }
        }

        $response = ['status' => '100', 'mensaje' => "Se agrego correctamente"];
        return   json_encode($response);
    }




}
