<?php

namespace App\Http\Controllers;

use App\Traits\ArticulosTrait;
use Illuminate\Http\Request;

use App\Http\Requests;

class ArticuloController extends Controller
{
    private $crud;
    public function __construct()
    {
        $this->middleware('auth');
        $this->crud = new ArticulosTrait();
    }
    public function articulos(Request $request){
        $articulosJson = $this->crud->articulos($request['pagina']);
        return view('articulos.articulos')->with('articulos',json_decode($articulosJson));
    }
    public function articulo(Request $request){
        $articuloJson = $this->crud->articulo($request['id']);
        return view('articulos.articulo')->with('articulo',json_decode($articuloJson));
    }
    public function nuevaArticulo(Request $request){
        $respuesta = $this->crud->nuevaArticulo($request);
        $articulo = json_decode($respuesta);
        $data = ["cantidad" => $request['cantidad'], "idarticulo" => $articulo->data->idarticulo ];
        $resp = $this->crud->entrada($data);
        return $respuesta;
    }
    public function editarArticulo(Request $request){
        $respuesta = $this->crud->editarArticulo($request);
        return $respuesta;
    }
    public function eliminarArticulo(Request $request){
        $respuesta = $this->crud->eliminarArticulo($request);
        return $respuesta;
    }

}
