<?php

namespace App\Http\Controllers;

use App\Traits\ArticulosTrait;
use App\Traits\HabitacionTrait;
use App\Traits\ClientesTrait;
use App\Traits\BookingTrait;
use Illuminate\Http\Request;

use App\Http\Requests;

class BookingController extends Controller
{
    private $habitacionCrud;
    private $clienteCrud;
    private $bookingCrud;
    private $articuloCrud;
    public function __construct()
    {
        $this->middleware('auth');
        $this->habitacionCrud = new HabitacionTrait();
        $this->clienteCrud = new ClientesTrait();
        $this->bookingCrud = new BookingTrait();
        $this->articuloCrud = new ArticulosTrait();
    }
    public function booking(Request $request){
        $habitacionesJson = $this->habitacionCrud->habitacionesLibres();
        return View('booking.booking')->with('habitaciones',json_decode($habitacionesJson));
    }
    public function bookings(Request $request){
        $habitacionesJson = $this->bookingCrud->bookings($request);
        return View('booking.bookings')->with('bookings',json_decode($habitacionesJson));
    }
    public function detalle(Request $request){
        $habitacionesJson = $this->bookingCrud->booking($request['idbooking']);
        $articulos = $this->articuloCrud->articulos_sp();
        return View('booking.detalle')->with('booking',json_decode($habitacionesJson))->with('articulos',json_decode($articulos));
    }
    public function caja(Request $request){
        $habitacionesJson = $this->bookingCrud->booking($request['idbooking']);
        return View('caja.detalle')->with('booking',json_decode($habitacionesJson));
    }
    public function pagar(Request $request){
        $response = $this->bookingCrud->pagar($request);
        return $response;
    }
    public function recibo(Request $request){
        $result = $this->bookingCrud->recibo($request['idbooking']);
        return View('caja.recibo')->with('booking',json_decode($result));
    }

    public function guardarBooking(Request $request){
        $jsonCliente = $this->clienteCrud->buscarCliente($request);
        $cliente = json_decode($jsonCliente);
        if($cliente->status != '200'){
            $this->clienteCrud->nuevaCliente($request);
        }
        $response = $this->bookingCrud->nuevaBooking($request);
        return $response;
    }
    public function editarBooking(Request $request){
        $response = $this->bookingCrud->editarBooking($request);
        return $response;
    }
    public function buscarCliente(Request $request){
        $jsonCliente = $this->clienteCrud->buscarCliente($request);
        return $jsonCliente;
    }

    public function eliminarBooking(Request $request){
        $response = $this->bookingCrud->eliminarBooking($request);
        return $response;
    }


}
