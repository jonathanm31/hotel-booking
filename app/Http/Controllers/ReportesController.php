<?php

namespace App\Http\Controllers;

use App\Traits\ArticulosTrait;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
class ReportesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ventas_productos(Request $request){
        $from = date("Y-m-d H:i:s",strtotime("-1 month"));
        $to = date("Y-m-d H:i:s",strtotime("month"));
        $ventas = DB::table('cajad')->join('articulos','cajad.idproducto','=','articulos.idarticulo')->where('cajad.tipo','=',1)->whereBetween('cajad.created_at', [$from, $to])->get();
        $response = (['status' => '200', 'mensaje' => 'Todas las articulos' , 'data' => $ventas]);
        return json_encode($response);
    }

    public function ventas_habitaciones(Request $request){
        $from ='2017-09-01';
        $to = '2018-12-01';
        $ventas = DB::table('cajad')->join('habitacion','cajad.idproducto','=','habitacion.idhabitacion')->where('cajad.tipo','=',0)->whereBetween('cajad.created_at', [$from, $to])->get();
        $response = (['status' => '200', 'mensaje' => 'Todas las habitaciones' , 'data' => $ventas]);
        return json_encode($response);
    }


}
