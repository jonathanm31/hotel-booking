<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PermisoUsuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(! Auth::user()) return redirect('login');
        $user = Auth::user()->idrol;

        switch ($user) {
            case '0':
               // es un usuario
                break;
            case '1':
                //return response('Unauthorized.', 401);
                break;
            default:
                return redirect('login');
                break;
        }



        return $next($request);
    }
}
