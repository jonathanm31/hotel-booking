<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    if(Auth::guest()){
        return redirect()->guest('login');
    }
    return view('home');
});



Route::get('denied',function(){
   return view('auth.permisos');
});
$this->get('logout', 'Auth\AuthController@logout');

Route::group(['middleware' => 'guest_group'],function(){
    $this->get('login', 'Auth\AuthController@showLoginForm');
    $this->post('login', 'Auth\AuthController@login');
    $this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    $this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    $this->post('password/reset', 'Auth\PasswordController@reset');
});


Route::group(['middleware' => 'admin_group'],function(){
    // Registration Routes...
    $this->get('register', 'Auth\AuthController@showRegistrationForm');
    $this->post('register', 'Auth\AuthController@register');

    $this->get('admin_panel' , 'HomeController@adminPanel');

    /*****STATS*******/
    $this->get('stats','EstadisticasController@index');

    /*******HABITACIONES*********/
    $this->get('habitaciones', 'HabitacionController@habitaciones');
    $this->get('habitacion', 'HabitacionController@habitacion');
    $this->get('nueva_habitacion', function(){
        return View('habitaciones.nueva_habitacion');
    });
    $this->post('nuevahabitacion', 'HabitacionController@nuevaHabitacion');
    $this->post('editarhabitacion', 'HabitacionController@editarHabitacion');
    $this->post('eliminarhabitacion', 'HabitacionController@eliminarHabitacion');
    $this->post('liberar','HabitacionController@liberarHabitacion');

    /******ARTICULOS*************/
    $this->get('articulos','ArticuloController@articulos');
    $this->get('articulo', 'ArticuloController@articulo');
    $this->get('nuevo_articulo', function(){
        return View('articulos.nuevo_articulo');
    });
    $this->post('nuevaarticulo', 'ArticuloController@nuevaArticulo');
    $this->post('editararticulo', 'ArticuloController@editarArticulo');
    $this->post('eliminararticulo', 'ArticuloController@eliminarArticulo');

    /********BOOKING***********************/
    $this->get('booking','BookingController@booking');
    $this->get('detalle','BookingController@detalle');
    $this->get('caja','BookingController@caja');
    $this->get('recibo','BookingController@recibo');
    $this->post('pagar','BookingController@pagar');
    $this->post('editar_booking','BookingController@editarBooking');

    $this->get('bookings','BookingController@bookings');
    $this->post('guardarbooking','BookingController@guardarBooking');
    $this->post('buscarcliente','BookingController@buscarCliente');
    $this->post('eliminarBooking','BookingController@eliminarBooking');

    /************INVENTARIO****************/
    $this->get('inventario','InventarioController@inventario');
    $this->post('movimiento',"InventarioController@moviemiento");

    /***********IMAGENES**********************/
    $this->post('guardar_imagen','HabitacionController@agregarImagen');
    $this->post('eliminar_imagen','HabitacionController@eliminarImagen');

    /*********REPORTES************************/
    $this->get('ventas_productos','ReportesController@ventas_productos');
    $this->get('ventas_habitaciones','ReportesController@ventas_habitaciones');
});


Route::group(['middleware' => 'usuario_group'],function(){

    Route::get('user_panel' , 'HomeController@userPanel');
});
