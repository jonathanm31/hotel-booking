<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CajaH extends Model
{
    protected $table = "cajah";
    protected $primaryKey = "idcaja";
    public $timestamps = false;
}
