<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Registros extends Model
{
    protected $table = "registro";
    protected $primaryKey = "idregistro";

    public $timestamps = false;
}
