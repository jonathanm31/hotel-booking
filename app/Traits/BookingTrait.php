<?PHP
namespace App\Traits;

use App\Models\Booking;
use App\Models\BookingD;
use App\Models\Habitacion;
use App\Models\Articulo;
use App\Models\CajaD;
use App\Models\CajaH;
use App\Models\Registros;
use Illuminate\Support\Facades\DB;
class BookingTrait {
    public function __construct(){

    }
    public function bookings($request){
        try{
            $pagina = $request['pagina'];
            $state = $request['state'];
            if($state == "")$state = 0;
            if(is_null($pagina)) $pagina = 15;
            //$bookings = Booking::simplePaginate($pagina);
            $bookings = DB::table('booking')
                        ->select('booking.*', 'cliente.nombres' , 'cliente.apellidos', 'cliente.dni')
                        ->where('booking.state','>',$state)
                        ->join('cliente','booking.idcliente','=','cliente.dni')->simplePaginate($pagina);

            if(count($bookings)<=0){
                $response = ['status' => '200', 'mensaje' => 'No hay bookings' , 'data' =>'0'];
                return json_encode($response);
            }
            $response = json_encode(['status' => '200', 'mensaje' => 'Todas las bookings' , 'data' => $bookings]);
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function booking($request){
        try{
            $idbooking = $request;
            $booking = DB::table('booking')->where('booking.idbooking','=',$idbooking)->join('cliente','idcliente','=','cliente.dni')->first();
            $detalle = DB::table('booking_detalle')
                ->select('booking_detalle.cantidad as cantidad_bk','booking_detalle.precio as precio_bk','booking_detalle.*','habitacion.*')
                ->where('booking_detalle.idbooking','=',$idbooking)
                ->join('habitacion','booking_detalle.idhabitacion','=','habitacion.idhabitacion')->get();
            $productos = DB::table('booking_detalle')
                ->select('articulos.nombre','articulos.descripcion','booking_detalle.*')
                ->where('booking_detalle.idbooking','=',$idbooking)
                ->where('booking_detalle.idhabitacion','=','0')
                ->join('articulos','booking_detalle.idarticulo','=','articulos.idarticulo')->get();

            $data = ['cabecera' => $booking, 'detalle' => $detalle,'productos' => $productos];

            $response = ['status' => '200', 'mensaje' => 'booking encontrada' , 'data' => $data];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function recibo($request){
        try{
            $idbooking = $request;
            $booking = DB::table('booking')->where('booking.idbooking','=',$idbooking)->join('cliente','idcliente','=','cliente.dni')->first();
            $detalle = DB::table('booking_detalle')->where('booking_detalle.idbooking','=',$idbooking)
                ->join('habitacion','booking_detalle.idhabitacion','=','habitacion.idhabitacion')->get();
            $productos = DB::table('booking_detalle')
                ->select('articulos.nombre','articulos.descripcion','booking_detalle.*')
                ->where('booking_detalle.idbooking','=',$idbooking)
                ->where('booking_detalle.idhabitacion','=','0')
                ->join('articulos','booking_detalle.idarticulo','=','articulos.idarticulo')->get();

            $caja = DB::table('cajah')->where('cajah.idbooking','=',$idbooking)->first();

            $data = ['cabecera' => $booking, 'detalle' => $detalle,'productos' => $productos,'caja' => $caja];

            $response = ['status' => '200', 'mensaje' => 'booking encontrada' , 'data' => $data];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function pagar($request){
        try{
            $idbooking = $request['idbooking'];
            $booking = Booking::find($idbooking);
            $booking->state = 2;
            $booking->save();
            $caja = new CajaH;
            $caja->idbooking = $idbooking;

            $caja->tipo_pago = $request['tipo_pago'];
            $caja->tipo_boleta = $request['tipo_boleta'];
            $num = DB::select('Select genNumeracion (?) as num ',array($request['tipo_boleta']));

            $caja->numeracion = $num[0]->num;

            $total = 0.0;
            $cantidad = 0;
            $caja->save();

            $detalles = DB::table('booking_detalle')->where('booking_detalle.idbooking','=',$idbooking)->get();
            foreach ($detalles as $detalle){
                $cajad = new CajaD;
                $cajad->idcajah = $caja->idcaja;
                if($detalle->idhabitacion == 0){
                    $cajad->idproducto = $detalle->idarticulo;
                    $cajad->tipo = 1;
                }else{
                    $cajad->idproducto = $detalle->idhabitacion;
                    $habitacion = Habitacion::find($detalle->idhabitacion);
                    $habitacion->state = 1 ;
                    $habitacion->save();
                    $cajad->tipo = 0;
                }
                $cajad->precio_unidad = $detalle->precio;
                $cajad->precio = $detalle->precio ;
                $cajad->cantidad = $detalle->cantidad;
                $total += $detalle->precio ;
                $cantidad += $detalle->cantidad;
                $cajad->save();
            }
            $igv = $total * 0.18;
            $caja->igv = $igv;
            $caja->descuento =($total * ($request['descuento']/100));
            $caja->total = $total - ($total * ($request['descuento']/100));
            $caja->cantidad = $cantidad;
            $caja->save();
            $response = ['status' => '200', 'mensaje' => 'Recibo Creado' , 'data' => $caja];
            return json_encode($response);
        }catch(Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function randomKey() {
        $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));
        $key = "";
        for($i=0; $i < 10; $i++) {
            $key .= $pool[mt_rand(0, count($pool) - 1)];
        }
        return $key;
    }
    public function nuevaBooking($request){
        try{
            $booking = new Booking;
            $booking->code = $this->randomKey();
            $booking->idcliente = $request['dni'];
            $booking->detalle = $request['detalle'];
            $booking->adultos = $request['adultos'];
            $booking->ninos = $request['ninos'];
            $booking->fec_ingreso = $request['fec_ingreso'];
            $booking->fec_salida = $request['fec_salida'];
            $booking->state = 1;
            $booking->save();

            $habitaciones = json_decode($request["habitaciones"]);
            foreach ($habitaciones as $habitacion){
                $bd = new BookingD;
                $bd->idbooking = $booking->idbooking;
                $bd->idhabitacion = $habitacion->idhabitacion;
                $bd->fec_ingreso = $request['fec_ingreso'];
                $bd->cantidad = $habitacion->cantidad;
                $bd->precio = $habitacion->precio;
                $bd->fec_salida = $request['fec_salida'];
                $bd->save();
                $habitacion = Habitacion::find($habitacion->idhabitacion);
                $habitacion->state = 0;
                $habitacion->save();
            }
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => ["idbooking" => $booking->idbooking]];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function editarBooking($request){
        try{
            $idbooking = $request['idbooking'];
            $booking = Booking::find($idbooking);
            $productos = json_decode($request["productos"]);
            foreach ($productos as $producto){
                $bd = new BookingD;
                $bd->idbooking = $idbooking;
                $bd->idhabitacion = 0;
                $bd->idarticulo = $producto->idproducto;
                $bd->fec_ingreso = $request['fec_ingreso'];
                $bd->cantidad = $producto->cantidad;
                $bd->precio = $producto->precio;
                $bd->fec_salida = $request['fec_salida'];
                $bd->save();
                $articulo = Articulo::find($producto->idproducto);
                $articulo->cantidad = $articulo->cantidad  - $producto->cantidad ;
                $articulo->save();

                $registro = new Registros;
                $registro->idbooking = $idbooking;
                $registro->idarticulo = $articulo->idarticulo;
                $registro->cantidad = $producto->cantidad;
                $registro->save();

            }
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => $booking];
            return json_encode($response);

        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function eliminarBooking($request){
        try{
            $idbooking = $request['idbooking'];
            Booking::find($idbooking)->delete();
            $response = ['status' => '200', 'mensaje' => 'booking eliminada' , 'data' => '0'];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
}