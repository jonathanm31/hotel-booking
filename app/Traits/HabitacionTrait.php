<?PHP
namespace App\Traits;

use App\Models\Habitacion;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
class HabitacionTrait {
    public function __construct(){

    }

    public function habitaciones($request){
        try{
            $pagina = $request;
            if(is_null($pagina)) $pagina = 15;
            $habitaciones = DB::table('habitacion')
                                    ->leftJoin('booking_detalle', function ($join) {
                                        $join->on('habitacion.idhabitacion', '=', 'booking_detalle.idhabitacion')
                                            ->where('booking_detalle.state', '>', 0);
                                    })
                                    ->select('habitacion.*', 'booking_detalle.fec_ingreso', 'booking_detalle.fec_salida')
                                    ->simplePaginate($pagina);
            if(count($habitaciones)<=0){
                $response = ['status' => '200', 'mensaje' => 'No hay habitaciones' , 'data' =>'0'];
                return json_encode($response);
            }
            $response = json_encode(['status' => '200', 'mensaje' => 'Todas las habitaciones' , 'data' => $habitaciones]);
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }

    }
    public function habitacionesLibres(){
        try{
            $habitaciones = Habitacion::select('idhabitacion','nombre','capacidad','state','precio')->where('state' ,'=', 1)->get();
            if(count($habitaciones)<=0){
                $response = ['status' => '200', 'mensaje' => 'No hay habitaciones' , 'data' =>'0'];
                return json_encode($response);
            }
            $response = json_encode(['status' => '200', 'mensaje' => 'Todas las habitaciones' , 'data' => $habitaciones]);
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }

    }
    public function habitacion($request){
        try{
            $idhabitacion = $request;
            $habitacion = Habitacion::find($idhabitacion);
            $response = ['status' => '200', 'mensaje' => 'habitacion encontrada' , 'data' => $habitacion];
            return json_encode($habitacion);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function nuevaHabitacion($request){
        try{
            $habitacion = new Habitacion;
            $habitacion->nombre = $request['nombre'];
            $habitacion->descripcion = $request['descripcion'];
            //$habitacion->imagenes = $request['descripcion'];
            $habitacion->precio = $request['precio'];
            $habitacion->capacidad = $request['capacidad'];
//            $habitacion->cantidad = $request['cantidad'];
            $habitacion->state = $request['state'];
            $habitacion->save();
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => json_encode($habitacion)];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function editarHabitacion($request){
        try{
            $idhabitacion = $request['idhabitacion'];
            $habitacion = Habitacion::find($idhabitacion);
            $habitacion->nombre = $request['nombre'];
            $habitacion->descripcion = $request['descripcion'];
            $habitacion->capacidad = $request['capacidad'];

            $habitacion->precio = $request['precio'];
            $habitacion->state = $request['state'];
            $habitacion->save();
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => json_encode($habitacion)];
            return json_encode($response);

        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function ocupada($request){
        try{
            $idhabitacion = $request['idhabitacion'];
            $habitacion = Habitacion::find($idhabitacion);
            $habitacion->state = 0;
            $habitacion->save();
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => json_encode($habitacion)];
            return json_encode($response);

        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function libre($request){
        try{
            $idhabitacion = $request['idhabitacion'];
            $habitacion = Habitacion::find($idhabitacion);
            $habitacion->state = 1;
            $habitacion->save();
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => json_encode($habitacion)];
            return json_encode($response);

        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function eliminarHabitacion($request){
        try{
            $idhabitacion = $request['idhabitacion'];
            $habitacion = Habitacion::find($idhabitacion)->delete();
            $response = ['status' => '200', 'mensaje' => 'habitacion eliminada' , 'data' => '0'];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function agregarImagen($request){
        try{
            $idhabitacion = $request['idhabitacion'];
            $imagen = $request['imagen'];
            $habitacion = Habitacion::find($idhabitacion);
            $imagenes =  json_decode($habitacion->imagenes);
            $imagenes[]= $imagen;
            $habitacion->imagenes = json_encode($imagenes);
            if($imagen->isFile()){
                $name = $imagen->getATime().".".$imagen->extension();
                Storage::put($name,$imagen);
                $response = ['status' => '200', 'mensaje' => 'imagen eliminada' , 'data' => ["nombre" => $name]];
                return json_encode($response);
            }else{
                $response = ['status' => '100', 'mensaje' => 'no es un archivo' , 'data' => '0'];
                return json_encode($response);
            }
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function eliminarImagen($request){
        try{
        $idhabitacion = $request['idhabitacion'];
        $imagen = $request['imagen'];
        $habitacion = Habitacion::find($idhabitacion);
        $imagenes =  json_decode($habitacion->imagenes);

        for($i = 0; $i < count($imagenes); $i++){
            if($imagenes[$i] == $imagen){
                unset($imagenes[$i]);
            }
        }
        $habitacion->imagenes = json_encode($imagenes);
        Storage::delete($imagen);
            $response = ['status' => '200', 'mensaje' => 'imagen eliminada' , 'data' => '0'];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
}