<?PHP
namespace App\Traits;

use App\Models\Articulo;
use App\Models\Registros;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
class ArticulosTrait {
    public function __construct(){

    }

    public function articulos($request){
        try{
            $pagina = $request;
            if(is_null($pagina)) $pagina = 15;
            $articulos = Articulo::simplePaginate($pagina);
            if(count($articulos)<=0){
                $response = ['status' => '200', 'mensaje' => 'No hay articulos' , 'data' =>'0'];
                return json_encode($response);
            }
            $response = json_encode(['status' => '200', 'mensaje' => 'Todas las articulos' , 'data' => $articulos]);
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }

    }
    public function articulos_sp(){
        try{
            $articulos = Articulo::get();
            if(count($articulos)<=0){
                $response = ['status' => '200', 'mensaje' => 'No hay articulos' , 'data' =>'0'];
                return json_encode($response);
            }
            $response = (['status' => '200', 'mensaje' => 'Todas las articulos' , 'data' => $articulos]);
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return ($response);
        }

    }
    public function articulosInventario(){
        $from = date('Y-m-d' . ' 00:00:00', time()); //need a space after dates.
        $to = date('Y-m-d' . ' 00:00:00',strtotime($from . "+1 days"));
        try{
            $articulos = DB::table('registro')
                        ->select('articulos.nombre','articulos.precio','registro.*')
                        ->join('articulos','registro.idarticulo','=','articulos.idarticulo')
                        ->whereBetween('registro.created_at', array($from, $to))->get();

            $response = (['status' => '200', 'mensaje' => 'Todas las articulos' , 'data' => $articulos, "from" => $from, "to" => $to]);
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return ($response);
        }
    }
    public function articulo($request){
        try{
            $idarticulo = $request;
            $articulo = Articulo::find($idarticulo);
            $response = ['status' => '200', 'mensaje' => 'articulo encontrada' , 'data' => $articulo];
            return json_encode($articulo);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function nuevaArticulo($request){
        try{
            $articulo = new Articulo;
            $articulo->nombre = $request['nombre'];
            $articulo->descripcion = $request['descripcion'];
            $articulo->cantidad = $request['cantidad'];
            $articulo->precio = $request['precio'];
            $articulo->costo = $request['costo'];
            $articulo->state = 1;
            $articulo->save();
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => $articulo];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function editarArticulo($request){
        try{
            $idarticulo = $request['idarticulo'];
            $articulo = Articulo::find($idarticulo);
            $articulo->nombre = $request['nombre'];
            $articulo->descripcion = $request['descripcion'];
            $articulo->precio = $request['precio'];
            $articulo->costo = $request['costo'];
            $articulo->state = $request['state'];
            $articulo->save();
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => $articulo];
            return json_encode($response);

        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function entrada($request){
        try{
            $cantidad = $request['cantidad'];
            $idarticulo = $request['idarticulo'];

            $articulo = Articulo::find($idarticulo);
            $articulo->cantidad = $articulo->cantidad + $cantidad;
            $articulo->save();

            $registro = new Registros;
            $registro->idarticulo = $idarticulo;
            $registro->cantidad = $cantidad;
            $registro->tipo = 1;
            $registro->save();

            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!'];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage()];
            return json_encode($response);
        }
    }
    public function salida($request){
        try{
            $cantidad = $request['cantidad'];
            $idarticulo = $request['idarticulo'];

            $articulo = Articulo::find($idarticulo);
            $articulo->cantidad = $articulo->cantidad - $cantidad;
            $articulo->save();

            $registro = new Registros;
            $registro->idarticulo = $idarticulo;
            $registro->cantidad = $cantidad;
            $registro->save();
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!'];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage()];
            return json_encode($response);
        }
    }
    public function eliminarArticulo($request){
        if($request['idarticulo'] != ""){
            $idarticulo = $request['idarticulo'];
            $articulo = Articulo::find($idarticulo)->delete();
            $response = ['status' => '200', 'mensaje' => 'articulo eliminada' , 'data' => '0'];
            return json_encode($response);
        }else {
            $response = ['status' => '100', 'mensaje' => 'upps, no hay articulo.' , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function agregarImagen($request){
        try{
            $idarticulo = $request['idarticulo'];
            $imagen = $request['imagen'];
            $articulo = Articulo::find($idarticulo);
            $imagenes =  json_decode($articulo->imagenes);
            $imagenes[]= $imagen;
            $articulo->imagenes = json_encode($imagenes);
            if($imagen->isFile()){
                $name = $imagen->getATime().".".$imagen->extension();
                Storage::put($name,$imagen);
                $response = ['status' => '200', 'mensaje' => 'imagen eliminada' , 'data' => ["nombre" => $name]];
                return json_encode($response);
            }else{
                $response = ['status' => '100', 'mensaje' => 'no es un archivo' , 'data' => '0'];
                return json_encode($response);
            }
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function eliminarImagen($request){
        try{
        $idarticulo = $request['idarticulo'];
        $imagen = $request['imagen'];
        $articulo = Articulo::find($idarticulo);
        $imagenes =  json_decode($articulo->imagenes);

        for($i = 0; $i < count($imagenes); $i++){
            if($imagenes[$i] == $imagen){
                unset($imagenes[$i]);
            }
        }
        $articulo->imagenes = json_encode($imagenes);
        Storage::delete($imagen);
            $response = ['status' => '200', 'mensaje' => 'imagen eliminada' , 'data' => '0'];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
}