<?PHP
namespace App\Traits;

use App\Models\Cliente;
use Illuminate\Support\Facades\Storage;
class ClientesTrait {
    public function __construct(){

    }

    public function clientes($request){
        try{
            $pagina = $request;
            if(is_null($pagina)) $pagina = 15;
            $clientes = Cliente::simplePaginate($pagina);
            if(count($clientes)<=0){
                $response = ['status' => '200', 'mensaje' => 'No hay clientes' , 'data' =>'0'];
                return json_encode($response);
            }
            $response = json_encode(['status' => '200', 'mensaje' => 'Todas las clientes' , 'data' => $clientes]);
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }

    }
    public function cliente($request){
        try{
            $idcliente = $request;
            $cliente = Cliente::find($idcliente);
            $response = ['status' => '200', 'mensaje' => 'cliente encontrada' , 'data' => $cliente];
            return json_encode($cliente);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function nuevaCliente($request){
        try{
            $cliente = new Cliente;
            $cliente->nombres = $request['nombres'];
            $cliente->apellidos = $request['apellidos'];
            $cliente->dni = $request['dni'];
            $cliente->ruc = $request['ruc'];
            $cliente->telefono = $request['telefono'];
            $cliente->direccion = $request['direccion'];
            $cliente->email = $request['email'];
            $cliente->state = 1;
            $cliente->save();
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => json_encode($cliente)];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function editarCliente($request){
        try{
            $idcliente = $request['idcliente'];
            $cliente = Cliente::find($idcliente);
            $cliente->nombres = $request['nombres'];
            $cliente->apellidos = $request['apellidos'];
            $cliente->dni = $request['dni'];
            $cliente->ruc = $request['ruc'];
            $cliente->telefono = $request['telefono'];
            $cliente->direccion = $request['direccion'];
            $cliente->email = $request['email'];
            $cliente->save();
            $response = ['status' => '200', 'mensaje' => 'Editado Correctamente!' , 'data' => $cliente];
            return json_encode($response);

        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function eliminarCliente($request){
        try{
            $idcliente = $request['idcliente'];
            $cliente = Cliente::find($idcliente)->delete();
            $response = ['status' => '200', 'mensaje' => 'cliente eliminada' , 'data' => '0'];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
    public function buscarCliente($request){
        try{
            $dni = $request['dni'];
            $cliente = Cliente::find($dni);
            $response = ['status' => '200', 'mensaje' => 'Se encontro Cliente' , 'data' => $cliente];
            return json_encode($response);
        }catch (Exception $e){
            $response = ['status' => '100', 'mensaje' => $e->getMessage() , 'data' => '0'];
            return json_encode($response);
        }
    }
}