-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.28-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_cuponera
CREATE DATABASE IF NOT EXISTS `db_cuponera` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_cuponera`;

-- Dumping structure for table db_cuponera.adminafiliado
CREATE TABLE IF NOT EXISTS `adminafiliado` (
  `idadminafiliado` int(11) NOT NULL AUTO_INCREMENT,
  `idadministrador` int(11) DEFAULT NULL,
  `idafiliado` varchar(250) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idadminafiliado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_cuponera.adminafiliado: ~0 rows (approximately)
/*!40000 ALTER TABLE `adminafiliado` DISABLE KEYS */;
/*!40000 ALTER TABLE `adminafiliado` ENABLE KEYS */;

-- Dumping structure for table db_cuponera.administrador
CREATE TABLE IF NOT EXISTS `administrador` (
  `idadministrador` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `apellidos` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT '0',
  `idrol` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idadministrador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_cuponera.administrador: ~0 rows (approximately)
/*!40000 ALTER TABLE `administrador` DISABLE KEYS */;
INSERT INTO `administrador` (`idadministrador`, `nombres`, `apellidos`, `password`, `email`, `idrol`, `created_at`, `updated_at`, `remember_token`) VALUES
	(22, 'harry', 'marquez', '$2y$10$c/MEwPjTYI7.y8LgYXQzRe/MGuqQkeQR92iEZkYWlm9BltLbYiSma', 'jonathanm31@gmail.com', 1, '2017-11-23 17:39:42', '2017-11-28 02:35:55', 'smLuyo36FVBexgqjzeTNBn0xSKpCBhWtGYBv08PCXNLoW8lFw67RAoIlpZfO'),
	(23, 'Jona', 'Marquez', '$2y$10$ITqgKHHKIoHodpyBNUHij.PcNUldLWP4NhKNxSZXnYufxWc1K4h.K', 'jonathanm31@gasmail.com', 0, '2017-11-23 17:48:29', '2017-11-23 18:31:11', 'lfCFqwBvxmevJMGmxEEfOqqRe5Irl7YAevVQrqnSpJGJu5kLxURwD3RJ3KcZ'),
	(25, 'harry', 'MArquez', '$2y$10$LVROjfF1JZ3SdfBAmU45Cepk1yWv7.DQ1uT1K.pAQHJhO9smzgvpS', 'jonathanm31@hmail.com', 0, '2017-11-23 23:46:22', '2017-11-24 02:22:24', '0yjhKxpVfaLOuznCJiqipVmxwjzQmqX2DySW3QLPm1TulEtOnZyMlmpbXMRN'),
	(26, 'Prof. Lavada Carroll DVM', 'Prof. Peyton Watsica II', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'heathcote.deshawn@example.org', 0, '2017-11-24 23:01:58', '2017-11-24 23:01:58', 'Iu8O6TT2Gf'),
	(27, 'Bernadette Rogahn', 'Mrs. Kailyn Stroman', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'zulauf.mollie@example.org', 0, '2017-11-24 23:01:58', '2017-11-24 23:01:58', 'Pu2N5rq4f3'),
	(28, 'Elizabeth Parker', 'Cornell Fisher MD', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'graham.janessa@example.org', 0, '2017-11-24 23:01:58', '2017-11-24 23:01:58', 'Se6hxEq9Q4'),
	(29, 'Chesley Trantow', 'Eddie Grimes MD', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'qreichel@example.net', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'NKnIpg1FTJ'),
	(30, 'Pasquale Kessler', 'Rosie Ebert', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'marcelina.pollich@example.org', 0, '2017-11-24 23:01:59', '2017-11-25 00:52:46', 'qvM7IzN0uTYfPUUU9xcsD0tvHTSsNyehG1rA1M6gxoFPAq3CCapnOEDjAI1E'),
	(31, 'Collin Jerde', 'Miss Lyda Marks', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'hmcclure@example.net', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'TChoEDxO3q'),
	(32, 'Leann Hackett', 'Miss Sister Rau', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'aschinner@example.net', 1, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'HqW1y9RnlR'),
	(33, 'Miss Sallie Berge', 'Archibald Medhurst', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'veum.katelin@example.org', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'ZchUBYarX5'),
	(34, 'Moises Heaney', 'Ella Kuhn', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'kuhlman.ed@example.com', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'EDZFL3jGBI'),
	(35, 'Cara Miller', 'Mr. Napoleon Green MD', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'mattie.hessel@example.com', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', '0Z1fZmDAit'),
	(36, 'Nikolas Collins', 'Arch Nienow I', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'donnelly.aryanna@example.net', 2, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'pJpiqmyc4P'),
	(37, 'Dr. Tanner Farrell Sr.', 'Ms. Maeve McGlynn', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'pouros.briana@example.net', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'UM4lCeczsn'),
	(38, 'Prof. Green Brown IV', 'Prof. Merl Greenfelder III', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'vlabadie@example.org', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'x7cFXATFSE'),
	(39, 'Hunter O\'Conner', 'Ms. Johanna Beatty', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'tfeest@example.com', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'yUlHNjd2ca'),
	(40, 'Dylan Gusikowski', 'Dr. Cristal Legros', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'hartmann.leonard@example.org', 2, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'BYtc2MKEcf'),
	(41, 'Harry Hahn', 'Orland Pouros Sr.', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'gleichner.gudrun@example.net', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'DVANHDAlOc'),
	(42, 'Xzavier Cremin', 'Deion Heaney', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'emurphy@example.org', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'U5sRn0jLXQ'),
	(43, 'Dr. Randal Wiegand Sr.', 'Prof. Devyn Smitham PhD', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'cassidy59@example.com', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'K3Hh3ZQIU9'),
	(44, 'Archibald Tillman DVM', 'Gino Schmeler', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'fadel.reva@example.net', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', 'gwogaje247'),
	(45, 'Casimer Predovic MD', 'Prof. Sonny Kulas V', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'pferry@example.com', 0, '2017-11-24 23:01:59', '2017-11-24 23:01:59', '0e4tjW0N0z'),
	(46, 'Dr. Nels Cremin', 'Kurt Abshire MD', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'vidal17@example.org', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'PYVXm0hA9F'),
	(47, 'Loy Dibbert', 'Russ Romaguera', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'gunnar.witting@example.com', 2, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'qpfVWY8147'),
	(48, 'Weston Adams', 'Xavier Ritchie Sr.', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'vernice06@example.com', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'l1qMF1PUkH'),
	(49, 'Mrs. Fannie Stamm DDS', 'Miss Leatha Bernhard', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'laisha.labadie@example.org', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'GplECCKz72'),
	(50, 'Abbie Schiller', 'Elena Pfannerstill', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'brady47@example.com', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'GBxln6uOKf'),
	(51, 'Haskell Jacobi', 'Tyler Hahn', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'dcarroll@example.com', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'weYG9SOAHn'),
	(52, 'Emelia Murray MD', 'Genoveva Goyette I', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'edison.wolff@example.com', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'k8sdgKJUyM'),
	(53, 'Ryley Hudson', 'Lucinda Spencer', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'terry.freeda@example.net', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'dtjrAwksPj'),
	(54, 'Michele Hayes', 'Hailey Heller', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'mcclure.jayme@example.net', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'Ut8vTftxi8'),
	(55, 'Miss Wilhelmine Bartell', 'Evan Leuschke', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'meagan.reinger@example.net', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'aD9PoQQAkw'),
	(56, 'Ethyl Russel', 'Joelle Ondricka', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'wstark@example.net', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'Sk0riwF9Gd'),
	(57, 'Freddie Reynolds MD', 'Lesley Upton V', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'alize37@example.org', 1, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'DoQ3qsnfJO'),
	(58, 'Mrs. Viva Frami IV', 'Zoila Bins DVM', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'nellie.ryan@example.org', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'OnM692BTX9'),
	(59, 'Maybelle Renner', 'Hans Gulgowski', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'vconnelly@example.net', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'QLmj4f27Kt'),
	(60, 'Terence Wiegand', 'Prof. Khalil Renner', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'zpollich@example.com', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'HaNTpw7Nas'),
	(61, 'Bert Keebler', 'Mr. Arden Schowalter DVM', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'kuvalis.jordyn@example.org', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'F82RPDJMrc'),
	(62, 'Justen Labadie', 'Moshe Gleason', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'cary.flatley@example.com', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'fCbrz12D5t'),
	(63, 'Ross Schuppe', 'Adrain Nicolas', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'caitlyn.mitchell@example.com', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'xTroDGGjNB'),
	(64, 'Dr. Lucius Grady PhD', 'Joseph Lowe', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'bettye26@example.org', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', 'r11pFPG8WS'),
	(65, 'Miss Natalie Eichmann Sr.', 'Mariane Rath', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'schuster.caterina@example.com', 0, '2017-11-24 23:02:00', '2017-11-24 23:02:00', '4L2klr01W1'),
	(66, 'Glenda Kilback', 'Dr. Tony Bode', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'vella30@example.org', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'SMRpx3GaXu'),
	(67, 'Ericka Labadie', 'Jay Mann', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'arielle.schroeder@example.org', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'KP2wvh4Mjt'),
	(68, 'Dr. Palma Stark I', 'Aletha Romaguera III', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'howell.dana@example.org', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'tqP0dXjoTt'),
	(69, 'Aditya Marvin', 'Joana Wehner', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'kovacek.hubert@example.net', 1, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'CP2bqFwdOB'),
	(70, 'Dr. Lucienne Lowe I', 'Nayeli Yundt', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'maverick.abshire@example.net', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'Ncl2qnCj46'),
	(71, 'Ruth Bartell', 'Alphonso Schumm', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'harvey.annabell@example.org', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', '6bljN6Lrp1'),
	(72, 'Prof. Selena Mitchell Jr.', 'Vada Daniel', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'purdy.tremaine@example.net', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'jGpe25mj9s'),
	(73, 'Dr. Gerald Pollich Jr.', 'Samir Graham', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'pfeffer.gerda@example.org', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'zUaTPPobR3'),
	(74, 'Dr. Johanna Friesen', 'Dr. Emmitt McKenzie', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'meta.swaniawski@example.com', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'lQMcjmmAgb'),
	(75, 'Tatum Kunze', 'Mrs. Nola Goodwin I', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'littel.nola@example.net', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'FQDdpsgqlN'),
	(76, 'Nelda Carter V', 'Prof. Iva Treutel MD', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'raina17@example.org', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', '23j27VRoai'),
	(77, 'Leonora Douglas', 'Mallory Huel', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'ratke.dolores@example.com', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', '1M4RiZ8N8F'),
	(78, 'Andy Schuster', 'Sherwood Klein', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'greg66@example.net', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'f0SoMrMffq'),
	(79, 'Ms. Danika Jacobi', 'Dr. Verner Larson', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'whills@example.net', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'jcOMioJ9iC'),
	(80, 'Luther Reilly Jr.', 'Dr. Gaston Keebler V', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'joesph67@example.net', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', '91h7japWC8'),
	(81, 'Mrs. Tara Beahan DDS', 'Miss Krystel Reichel', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'haley.laury@example.net', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'fukiGh3lw7'),
	(82, 'Rae Rolfson', 'Virgil Rau DDS', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'reece38@example.net', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'VoXXPRGxOu'),
	(83, 'Dr. Esmeralda Klein', 'Caleb Block', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'fadel.beulah@example.org', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', '2r86HufHie'),
	(84, 'Maverick Reichert', 'Dr. Randal Spencer', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'reyna32@example.com', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', '2wa71tL69L'),
	(85, 'Tomasa Rath', 'Dr. Esmeralda Bergnaum', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'dach.opal@example.net', 0, '2017-11-24 23:02:01', '2017-11-24 23:02:01', 'J4hK7g4LeT'),
	(86, 'Serena Cummings', 'Milan DuBuque', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'nils.boyle@example.net', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'H8WXCzBOfT'),
	(87, 'Trey VonRueden PhD', 'Mr. Brain O\'Keefe PhD', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'ryan.garett@example.net', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'krzvPJudsS'),
	(88, 'Jedidiah Barton', 'Prof. Claudine Kub', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'nader.mohammed@example.org', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'cGV4EHnQ0W'),
	(89, 'Dr. Walton Spencer Sr.', 'Marcus Senger', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'oda.christiansen@example.com', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'c3uL0dDupj'),
	(90, 'Flavio Spencer', 'Lisandro Langworth', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'ubayer@example.net', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'VBTXaVsUEP'),
	(91, 'Kaci Rodriguez', 'Mrs. Annamarie Emard', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'ruben.kreiger@example.org', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'jXBR2V8uHN'),
	(92, 'Ms. Nola Schneider MD', 'Bonnie Raynor', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'savannah.dare@example.org', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'eRpCRRAdOu'),
	(93, 'Prof. Maximillia Cormier Sr.', 'Prof. Monserrate Towne I', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'bettye.lesch@example.net', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'JkxGNPUNDc'),
	(94, 'Sierra Hartmann', 'Delfina Cassin III', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'iquitzon@example.net', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', '9h8VaXmXAh'),
	(95, 'Brody Hintz', 'Ms. Alba Hoeger PhD', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'maia.spencer@example.org', 2, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'P9tSCrm31Z'),
	(96, 'Prof. Mona Yundt II', 'Mr. Mavis Beatty Jr.', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'heller.dariana@example.net', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'YPG1amRDPq'),
	(97, 'Prof. Ollie Funk', 'Yolanda Graham', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'glenda.crona@example.org', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'NDjfRIdfeV'),
	(98, 'Prof. Noemy Boehm', 'Viola Ferry', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'santino26@example.com', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'aqzW7EfFA3'),
	(99, 'Prof. Sherman Bernhard', 'Mrs. Ines Rempel DDS', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'catharine.jast@example.org', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'W9LpOfwQMO'),
	(100, 'Otha Lang', 'Warren Upton', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'johnny.rempel@example.com', 2, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'TrBzvHOn5J'),
	(101, 'Rosemarie Pagac DVM', 'Prof. Florencio Carter V', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'rusty.bashirian@example.org', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'Fp4YETHCeZ'),
	(102, 'Kayli Carroll', 'Carrie Thiel III', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'johnathon.parker@example.org', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'Yi7vH5VuEP'),
	(103, 'Pink Yost', 'Yvonne Beer', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'ydooley@example.org', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'NfO0HtTAby'),
	(104, 'Andy Morar', 'Herta Wyman', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'kirlin.ena@example.org', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'aiB8egUhAr'),
	(105, 'Dr. Cecelia Mohr I', 'Mr. Robb Gibson I', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'swilliamson@example.net', 0, '2017-11-24 23:02:02', '2017-11-24 23:02:02', 'aSPCIWFBI0'),
	(106, 'Dr. Elbert Heathcote III', 'Ilene Abshire', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'franecki.matilda@example.org', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', '6wPe40zOxV'),
	(107, 'Sonny Schoen PhD', 'Keshaun Schaden', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'kunze.lucy@example.net', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', '4N1imYaFNW'),
	(108, 'Loyce Ullrich PhD', 'Alfonso Moen', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'wendell.graham@example.net', 2, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'lPG9WTgzlU'),
	(109, 'Sanford Gutmann I', 'Brenda Becker Sr.', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'kyle.goodwin@example.net', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'MLer0VQ9Oa'),
	(110, 'Mr. Alden Russel Jr.', 'Sigmund Schiller', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'zryan@example.net', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'lakPHCS0WA'),
	(111, 'Mary Reilly', 'Mr. Ransom Howell MD', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'anissa52@example.org', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'iMP0K130Wd'),
	(112, 'Mr. Brenden Keeling Sr.', 'Prof. Jaclyn Hoeger DDS', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'qgleason@example.com', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'RQXSg68RQW'),
	(113, 'Margret Glover', 'Prof. Jon Moen I', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'kareem.ortiz@example.org', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', '8XfRv1cQen'),
	(114, 'Miss Ciara Tremblay DVM', 'Prof. Curtis Reilly', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'alta.kuhic@example.net', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'LR8Iikz8OY'),
	(115, 'Eduardo Gerhold', 'Elna Herzog', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'dena99@example.net', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', '4V5ojO6qXV'),
	(116, 'Randall Wisozk', 'Keira Muller III', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'petra.bogan@example.com', 1, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'OXwNG9Gmak'),
	(117, 'Dr. Jessie Donnelly MD', 'Dr. Jayce Lang', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'bpacocha@example.com', 1, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'rzzjLWNz71'),
	(118, 'Sasha Schmeler', 'Mr. Antwon Cruickshank Sr.', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'daniella76@example.org', 0, '2017-11-24 23:02:03', '2017-11-24 23:51:19', 'UZCXVXNdGPj3sR64DJ3w38W2NXrKXzQXISI23l4gb8OcYoUfuzziqko2RNk2'),
	(119, 'Lamar Johnston', 'Aiden Rolfson', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'pschulist@example.com', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'xKuhFGYmm7'),
	(120, 'Emerald Crooks', 'Kelly McLaughlin', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'osenger@example.net', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'N7tMupaMsK'),
	(121, 'Prof. Beth Keebler', 'Mrs. Linda Grant', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'witting.tomas@example.net', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'BtJmjHLh9p'),
	(122, 'Leonardo Morissette', 'Moriah Zemlak', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'brandyn.ledner@example.com', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', '7AEYvUSsl9'),
	(123, 'Garrett Erdman', 'Humberto Renner', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'erin83@example.com', 0, '2017-11-24 23:02:03', '2017-11-24 23:02:03', 'saIWeBvZrV'),
	(124, 'Damien Rosenbaum', 'Dangelo Gutmann', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'lschoen@example.com', 1, '2017-11-24 23:02:04', '2017-11-24 23:02:04', '8i0qE1EtQZ'),
	(125, 'Miss Valentina Herman Jr.', 'Destiney Schneider', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'dibbert.yolanda@example.com', 0, '2017-11-24 23:02:04', '2017-11-24 23:02:04', 'fGaeI60PCS'),
	(126, 'Juan ', 'Perez', '$2y$10$q/1GXtXQkJGd279/JBxuAumzl3XfIVZI2VgTS2WzRbneec/QgFbei', 'juan@gmail.com', 0, '2017-11-25 01:24:17', '2017-11-25 01:26:10', 'TpeA5Knz8SiLt6MTZIAscEk0jZ2LHbvePcT93PNBzj6tXDTR0u4gx6AC3kqL'),
	(127, 'juanjo', 'Paco', '$2y$10$m0VfQcQPMnCacoS2FapfS.MDE/G4pU5NVEAyh75sqS0Ah1ZuYOZDS', 'jonathanm31@jmail.com', 0, '2017-11-25 03:42:52', '2017-11-28 00:06:12', 'npNZg3PcC3HO5z87JlVEGfVM4ODoxhAoshIHS4FfmCn3d3xkMhh8FkV0LTYu'),
	(128, 'juanjo', 'Paco', '$2y$10$Pu9nDFTeJDf85YY1KX2G3.yrE1.HvSy1u.cZEm8Ps335Qu9FUOzdK', 'jonathanm31@amail.com', 0, '2017-11-25 03:55:18', '2017-11-25 03:55:18', NULL);
/*!40000 ALTER TABLE `administrador` ENABLE KEYS */;

-- Dumping structure for table db_cuponera.admi_empresa
CREATE TABLE IF NOT EXISTS `admi_empresa` (
  `idadmiempresa` int(11) NOT NULL AUTO_INCREMENT,
  `idadministrador` int(11) DEFAULT NULL,
  `idempresa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idadmiempresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_cuponera.admi_empresa: ~0 rows (approximately)
/*!40000 ALTER TABLE `admi_empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `admi_empresa` ENABLE KEYS */;

-- Dumping structure for table db_cuponera.afiliados
CREATE TABLE IF NOT EXISTS `afiliados` (
  `idafiliado` int(11) NOT NULL AUTO_INCREMENT,
  `dependiente` varchar(250) NOT NULL COMMENT 'hijo, esposa, dependiente de idafiliado',
  `parentesco` varchar(250) NOT NULL,
  `ape_materno` varchar(250) NOT NULL,
  `ape_paterno` varchar(250) NOT NULL,
  `nombres` varchar(250) NOT NULL,
  `tipo_doc` varchar(250) DEFAULT NULL COMMENT '1=dni',
  `num_doc` varchar(250) DEFAULT NULL,
  `tiposeguro` int(11) DEFAULT NULL COMMENT '1=plan salud, 2=sctr, 3=vida ley, 4=otros seguros',
  `idempresa` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `token` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idafiliado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_cuponera.afiliados: ~0 rows (approximately)
/*!40000 ALTER TABLE `afiliados` DISABLE KEYS */;
/*!40000 ALTER TABLE `afiliados` ENABLE KEYS */;

-- Dumping structure for table db_cuponera.cuponafiliado
CREATE TABLE IF NOT EXISTS `cuponafiliado` (
  `idcuponafiliado` int(11) NOT NULL AUTO_INCREMENT,
  `idafiliado` varchar(250) DEFAULT NULL,
  `idcupon` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT '0',
  `state` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idcuponafiliado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_cuponera.cuponafiliado: ~0 rows (approximately)
/*!40000 ALTER TABLE `cuponafiliado` DISABLE KEYS */;
/*!40000 ALTER TABLE `cuponafiliado` ENABLE KEYS */;

-- Dumping structure for table db_cuponera.cupones
CREATE TABLE IF NOT EXISTS `cupones` (
  `idcupon` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `cantidad_afiliado` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idcupon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_cuponera.cupones: ~0 rows (approximately)
/*!40000 ALTER TABLE `cupones` DISABLE KEYS */;
/*!40000 ALTER TABLE `cupones` ENABLE KEYS */;

-- Dumping structure for table db_cuponera.empresas
CREATE TABLE IF NOT EXISTS `empresas` (
  `idempresa` int(11) NOT NULL AUTO_INCREMENT,
  `idbroker` int(11) NOT NULL,
  `empresa` varchar(200) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `color` varchar(250) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=1014 DEFAULT CHARSET=utf8;

-- Dumping data for table db_cuponera.empresas: ~91 rows (approximately)
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
INSERT INTO `empresas` (`idempresa`, `idbroker`, `empresa`, `logo`, `color`, `parent_id`, `state`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Grupo GyM', '1.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(2, 7, 'HUDBAY', '2.png', '#d11d22', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(3, 7, 'Oncosalud', '3.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(4, 7, 'Rimac', '4.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(5, 1, 'Grupo ICCGSA', '5.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(6, 1, 'San Martin', '6.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(7, 1, 'Andrade', '7.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(8, 1, 'Mondelez', '8.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(9, 7, 'Scotiabank', '9.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(10, 7, 'Centro Odontologico Americano (COA)', '10.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(11, 7, 'Mapfre', '11.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(12, 7, 'Teleatento', '12.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(13, 1, 'Kimberly Clark', '13.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(14, 1, 'Avianca', '14.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(15, 1, 'AON Graña Corredores de Seguros', '15.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(16, 7, 'Wayra', '16.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(17, 1, 'Indeco', '17.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(18, 7, 'Contacto Corredores de Seguros', '18.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(19, 7, 'Telefonica', '19.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(20, 8, 'Grupo Corona', '20.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(21, 1, 'Haug', '21.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(22, 1, 'Celima', '22.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(23, 1, 'Everis', '23.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(24, 7, 'Master Seguros Corredores de Seguros', '24.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(25, 7, 'Interbank', '25.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(26, 7, 'Firth Industries', '26.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(27, 7, 'El Comercio', '27.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(28, 7, 'BCP', '28.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(29, 7, 'Softline International', '29.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(30, 7, 'Motorex', '30.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(31, 7, 'Pacifico', '31.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(32, 7, 'JLT Corredores de Seguros', '32.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(33, 1, 'Ernst & Young', '33.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(34, 1, 'Lima Tours', '34.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(35, 7, 'Banco Financiero', '35.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(36, 7, 'Marsh Corredores de Seguros', '36.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(37, 7, 'Clinica San Pablo', '37.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(38, 7, 'Clinica Internacional', '38.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(39, 7, 'Los Portales', '39.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(40, 7, 'Entel', '40.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(41, 7, 'Smartclick', '41.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(42, 7, 'Productos de Acero Cassado', '42.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(43, 7, 'Staff Mktg Publicidad y Diseño', '43.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(44, 7, 'Sistemas Analiticos', '44.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(45, 7, 'Supermercados Peruanos (SPSA)', '45.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(46, 1, 'Statkraft Peru', '46.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(47, 7, 'Fandango', '47.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(48, 7, 'Sanitas Peru', '48.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(49, 7, 'Quantico Trends', '49.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(50, 1, 'Las Bambas', '50.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(51, 7, 'INRETAIL MANAGEMENT ', '51.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(52, 7, 'Willis Towers Watson', '52.png', '#702082', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(53, 7, 'La Positiva Seguros', '53.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(54, 7, 'Navitranso', '54.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(55, 1, 'Grupo Backus', '55.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(56, 1, 'Cencosud', '56.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(57, 7, 'Financiera Uno', '57.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(58, 7, 'Buenaventura', '58.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(59, 7, 'Belatrix Software', '59.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(60, 7, 'Corporación Furukawa', '60.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00'),
	(61, 1, 'Consorcio para la atención y mantenimientos de ductos del Perú\r\n', '61.png', '#00bfd6', NULL, 0, '2017-08-04 09:57:28', '0000-00-00 00:00:00'),
	(62, 2, 'Superintendencia de Banca y Seguros', '62.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(63, 2, 'Prosegur', '63.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(64, 2, 'Volvo', '64.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(65, 2, 'Skberge', '65.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(66, 2, 'Accenture Peru', '66.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(67, 2, 'Tecnofarma', '67.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(68, 2, 'Diebold', '68.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(69, 2, 'Editora Perú', '69.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(70, 2, 'Inchcape', '70.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(71, 2, 'Verizon Perú', '71.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(72, 2, 'Tecnocom', '72.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(73, 2, 'Vertiv Perú', '73.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(74, 2, 'FONAFE', '74.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(75, 2, 'E-Buisness Distribution Peru S.A.', '75.png', '#702082', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(76, 1, 'Grupo Flesan', '76.png', '#00bfd6', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(77, 3, 'Danper Trujillo', '77.png', '#00bfd6', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(78, 3, 'Owens', '78.png', '#00bfd6', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(79, 7, 'Silvestre Peru', '79.png', '#00bfd6', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(80, 3, 'Cabify', '80.png', '#00bfd6', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(81, 3, 'Asociacion Cultural Peruano Britanica\r\n', '81.png', '#00bfd6', NULL, 0, '2017-08-16 14:01:26', '0000-00-00 00:00:00'),
	(82, 2, 'Aceros Arequipa', '82.png', '#702082', NULL, 0, '2017-11-02 08:24:08', '0000-00-00 00:00:00'),
	(83, 2, 'Banbif', '83.png', '#702082', NULL, 0, '2017-11-02 08:24:08', '0000-00-00 00:00:00'),
	(84, 2, 'Ericksson', '84.png', '#702082', NULL, 0, '2017-11-02 08:24:08', '0000-00-00 00:00:00'),
	(85, 2, 'Hersil', '85.png', '#702082', NULL, 0, '2017-11-02 08:24:08', '0000-00-00 00:00:00'),
	(86, 2, 'Bancolombia', '86.png', '#702082', NULL, 0, '2017-11-02 08:24:08', '0000-00-00 00:00:00'),
	(87, 3, 'Divemotor', '87.png', '#00bfd6', NULL, 0, '2017-11-02 08:24:08', '0000-00-00 00:00:00'),
	(88, 3, 'Grupo Redondos', '88.png', '#00bfd6', NULL, 0, '2017-11-02 08:24:08', '0000-00-00 00:00:00'),
	(89, 3, 'Grupo Sandoval', '89.png', '#00bfd6', NULL, 0, '2017-11-02 08:24:08', '0000-00-00 00:00:00'),
	(90, 1, 'Miski Mayo', '90.png', '#00bfd6', NULL, 0, '2017-11-02 08:24:08', '0000-00-00 00:00:00'),
	(1013, 7, 'Mapsalud', '1013.png', '#00bfd6', NULL, 0, '2017-07-18 16:13:23', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;

-- Dumping structure for table db_cuponera.log
CREATE TABLE IF NOT EXISTS `log` (
  `idlog` int(11) NOT NULL AUTO_INCREMENT,
  `accion` int(11) DEFAULT NULL COMMENT '(0->aceptar solicitud , etc)',
  `idadministrador` int(11) DEFAULT NULL,
  `idafiliado` int(11) DEFAULT NULL,
  `idcupon` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idlog`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_cuponera.log: ~0 rows (approximately)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- Dumping structure for table db_cuponera.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `idmessage` int(11) NOT NULL AUTO_INCREMENT,
  `idcupon` int(11) DEFAULT NULL,
  `idafiliado` varchar(250) DEFAULT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `message` text,
  `state` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idmessage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_cuponera.messages: ~0 rows (approximately)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Dumping structure for table db_cuponera.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `idrol` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `state` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Dumping data for table db_cuponera.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`idrol`, `tipo`, `nombre`, `state`) VALUES
	(1, 1, 'ADMINISTRADOR', 1),
	(2, 0, 'USUARIO', 1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
