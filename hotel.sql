-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.28-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_hotel.administrador
CREATE TABLE IF NOT EXISTS `administrador` (
  `idadministrador` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `apellidos` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT '0',
  `serie` varchar(50) DEFAULT '0',
  `idrol` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idadministrador`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.administrador: ~1 rows (approximately)
/*!40000 ALTER TABLE `administrador` DISABLE KEYS */;
INSERT INTO `administrador` (`idadministrador`, `nombres`, `apellidos`, `password`, `email`, `serie`, `idrol`, `created_at`, `updated_at`, `remember_token`) VALUES
	(1, 'Leann Hackett', 'Miss Sister Rau', '$2y$10$Hfv9dI/s7PBA4dV2rc1HouAUuCVxoMy9ca0OBM71vEWCuM8k7LsBK', 'jonathanm31@gmail.com', '0', 1, NULL, '2018-03-08 20:37:50', 'jghy1jA0h1yzegLaW02XeqquZheMrYWKkO6tBCNDHc0VrdOn8uNX210awBdH');
/*!40000 ALTER TABLE `administrador` ENABLE KEYS */;

-- Dumping structure for table db_hotel.articulos
CREATE TABLE IF NOT EXISTS `articulos` (
  `idarticulo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '0',
  `descripcion` varchar(100) NOT NULL DEFAULT '0',
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `precio` float NOT NULL DEFAULT '0',
  `costo` float NOT NULL DEFAULT '0',
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idarticulo`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.articulos: ~3 rows (approximately)
/*!40000 ALTER TABLE `articulos` DISABLE KEYS */;
INSERT INTO `articulos` (`idarticulo`, `nombre`, `descripcion`, `cantidad`, `precio`, `costo`, `state`, `created_at`, `updated_at`) VALUES
	(2, 'Jabon ', 'Jabon de Tocador', 20, 2.63, 1.2, 1, '2018-03-08 20:52:59', '2018-05-02 18:26:25'),
	(3, 'Toalla', 'Toalla de mano', 34, 10.2, 5.3, 1, '2018-03-09 04:48:58', '2018-05-02 04:40:47'),
	(13, 'Inkacola 200ml', '', 19, 12, 2.2, 1, '2018-05-02 04:14:33', '2018-05-02 04:40:47');
/*!40000 ALTER TABLE `articulos` ENABLE KEYS */;

-- Dumping structure for table db_hotel.booking
CREATE TABLE IF NOT EXISTS `booking` (
  `idbooking` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `idcliente` varchar(50) NOT NULL,
  `adultos` int(11) NOT NULL,
  `ninos` int(11) NOT NULL,
  `personas` int(11) NOT NULL,
  `detalle` text,
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '0 anulado,1 activo , 2 cancelado',
  `fec_ingreso` datetime NOT NULL,
  `fec_salida` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`idbooking`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.booking: ~2 rows (approximately)
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` (`idbooking`, `code`, `idcliente`, `adultos`, `ninos`, `personas`, `detalle`, `state`, `fec_ingreso`, `fec_salida`, `created_at`, `updated_at`) VALUES
	(1, 'FkiwuNALBZ', '45856598', 0, 0, 0, '', 2, '2018-05-01 22:05:29', '2018-05-15 22:06:29', '2018-04-30 22:10:46', '2018-05-01 03:10:46'),
	(2, 'KmQi15ct5V', '45856598', 0, 0, 0, '', 2, '2018-05-01 22:19:18', '2018-05-22 22:20:18', '2018-04-30 22:34:35', '2018-05-01 03:34:35'),
	(3, 'FZs8v1ZF8W', '45856598', 0, 0, 0, '', 2, '2018-05-01 22:13:00', '2018-05-16 22:14:00', '2018-05-02 11:04:37', '2018-05-02 16:04:37');
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;

-- Dumping structure for table db_hotel.booking_detalle
CREATE TABLE IF NOT EXISTS `booking_detalle` (
  `idbookingd` int(11) NOT NULL AUTO_INCREMENT,
  `idbooking` int(11) NOT NULL,
  `idhabitacion` int(11) NOT NULL COMMENT '0 cuando no es producto',
  `idarticulo` int(11) NOT NULL DEFAULT '0' COMMENT '0 cuando no es habitacion',
  `cantidad` int(11) NOT NULL DEFAULT '1',
  `precio` float NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '1',
  `fec_ingreso` datetime DEFAULT NULL,
  `fec_salida` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idbookingd`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.booking_detalle: ~8 rows (approximately)
/*!40000 ALTER TABLE `booking_detalle` DISABLE KEYS */;
INSERT INTO `booking_detalle` (`idbookingd`, `idbooking`, `idhabitacion`, `idarticulo`, `cantidad`, `precio`, `state`, `fec_ingreso`, `fec_salida`, `created_at`, `updated_at`) VALUES
	(1, 1, 7, 0, 1, 12, 1, '2018-05-01 22:05:29', '2018-05-15 22:06:29', '2018-05-01 03:05:46', '2018-05-01 03:05:46'),
	(2, 1, 8, 0, 1, 32, 1, '2018-05-01 22:05:29', '2018-05-15 22:06:29', '2018-05-01 03:05:46', '2018-05-01 03:05:46'),
	(3, 1, 0, 2, 1, 2.63, 1, NULL, NULL, '2018-05-01 03:06:00', '2018-05-01 03:06:00'),
	(4, 2, 7, 0, 1, 12, 1, '2018-05-01 22:19:18', '2018-05-22 22:20:18', '2018-05-01 03:19:27', '2018-05-01 03:19:27'),
	(5, 3, 3, 0, 2, 12.56, 1, '2018-05-01 22:13:00', '2018-05-16 22:14:00', '2018-05-02 03:13:14', '2018-05-02 03:13:14'),
	(6, 3, 0, 2, 1, 2.63, 1, NULL, NULL, '2018-05-02 03:13:26', '2018-05-02 03:13:26'),
	(7, 3, 0, 2, 1, 2.63, 1, NULL, NULL, '2018-05-02 03:13:40', '2018-05-02 03:13:40'),
	(8, 3, 0, 2, 1, 2.63, 1, NULL, NULL, '2018-05-02 03:14:25', '2018-05-02 03:14:25'),
	(9, 3, 0, 2, 1, 2.63, 1, NULL, NULL, '2018-05-02 03:15:22', '2018-05-02 03:15:22');
/*!40000 ALTER TABLE `booking_detalle` ENABLE KEYS */;

-- Dumping structure for table db_hotel.cajad
CREATE TABLE IF NOT EXISTS `cajad` (
  `idcajad` int(11) NOT NULL AUTO_INCREMENT,
  `idcajah` int(11) NOT NULL DEFAULT '0',
  `descuento` int(11) DEFAULT '0',
  `idproducto` int(11) DEFAULT '0' COMMENT 'puede ser habitacion o producto',
  `tipo` int(11) DEFAULT '0' COMMENT '0 habitacion, 1 producto',
  `cantidad` int(11) DEFAULT '0',
  `precio_unidad` float DEFAULT NULL,
  `precio` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcajad`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.cajad: ~9 rows (approximately)
/*!40000 ALTER TABLE `cajad` DISABLE KEYS */;
INSERT INTO `cajad` (`idcajad`, `idcajah`, `descuento`, `idproducto`, `tipo`, `cantidad`, `precio_unidad`, `precio`, `created_at`) VALUES
	(1, 1, 0, 7, 0, 1, 12, 12, '2018-04-30 22:10:46'),
	(2, 1, 0, 8, 0, 1, 32, 32, '2018-04-30 22:10:46'),
	(3, 1, 0, 2, 1, 1, 2.63, 2.63, '2018-04-30 22:10:46'),
	(4, 2, 0, 7, 0, 1, 12, 12, '2018-04-30 22:34:35'),
	(5, 3, 0, 3, 0, 2, 12.56, 25.12, '2018-05-02 11:04:38'),
	(6, 3, 0, 2, 1, 1, 2.63, 2.63, '2018-05-02 11:04:38'),
	(7, 3, 0, 2, 1, 1, 2.63, 2.63, '2018-05-02 11:04:38'),
	(8, 3, 0, 2, 1, 1, 2.63, 2.63, '2018-05-02 11:04:38'),
	(9, 3, 0, 2, 1, 1, 2.63, 2.63, '2018-05-02 11:04:38');
/*!40000 ALTER TABLE `cajad` ENABLE KEYS */;

-- Dumping structure for table db_hotel.cajah
CREATE TABLE IF NOT EXISTS `cajah` (
  `idcaja` int(11) NOT NULL AUTO_INCREMENT,
  `idbooking` int(11) NOT NULL,
  `tipo_boleta` int(11) DEFAULT NULL COMMENT '0 boleta, 1 factura',
  `tipo_pago` int(11) DEFAULT NULL COMMENT '0 efectivo, 1 visa, 2 mastercard',
  `descuento` float DEFAULT NULL,
  `numeracion` int(11) NOT NULL,
  `total` float DEFAULT NULL,
  `igv` float DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcaja`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.cajah: ~2 rows (approximately)
/*!40000 ALTER TABLE `cajah` DISABLE KEYS */;
INSERT INTO `cajah` (`idcaja`, `idbooking`, `tipo_boleta`, `tipo_pago`, `descuento`, `numeracion`, `total`, `igv`, `cantidad`, `state`, `created_at`) VALUES
	(1, 1, 0, 0, 0, 1, 46.63, 8.3934, 3, 1, '2018-04-30 22:10:46'),
	(2, 2, 0, 0, 0, 2, 12, 2.16, 1, 1, '2018-04-30 22:34:35'),
	(3, 3, 1, 1, 0, 1, 35.64, 6.4152, 6, 1, '2018-05-02 11:04:38');
/*!40000 ALTER TABLE `cajah` ENABLE KEYS */;

-- Dumping structure for table db_hotel.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `dni` varchar(15) NOT NULL DEFAULT '0',
  `ruc` varchar(20) NOT NULL DEFAULT '0',
  `apellidos` varchar(100) NOT NULL DEFAULT '0',
  `nombres` varchar(100) NOT NULL DEFAULT '0',
  `telefono` varchar(100) DEFAULT '0',
  `direccion` varchar(100) DEFAULT '0',
  `email` varchar(100) DEFAULT '0',
  `ubicacion` varchar(100) DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.cliente: ~0 rows (approximately)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`dni`, `ruc`, `apellidos`, `nombres`, `telefono`, `direccion`, `email`, `ubicacion`, `state`, `created_at`, `updated_at`) VALUES
	('45856598', '', 'Marquez Muñoz', 'Harry', '225544', 'Campo Verde J-14 Sachaca', 'jonathanm31@gmail.com', '0', 1, '2018-03-09 01:48:18', '2018-03-09 01:48:18');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Dumping structure for table db_hotel.comentario
CREATE TABLE IF NOT EXISTS `comentario` (
  `idcomentario` int(11) NOT NULL AUTO_INCREMENT,
  `idhabitacion` int(11) DEFAULT '0',
  `comentario` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcomentario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.comentario: ~0 rows (approximately)
/*!40000 ALTER TABLE `comentario` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentario` ENABLE KEYS */;

-- Dumping structure for function db_hotel.genNumeracion
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `genNumeracion`(
	`tipo` INT

) RETURNS int(11)
    DETERMINISTIC
    COMMENT 'obtiene el ultimo numero del tipo de boleta'
BEGIN
DECLARE num INT(11) DEFAULT 0;
SET num = (SELECT COALESCE(MAX(cajah.numeracion),0)
FROM cajah
WHERE cajah.state = 1 and cajah.tipo_boleta = tipo);
RETURN num  + 1;
END//
DELIMITER ;

-- Dumping structure for table db_hotel.habitacion
CREATE TABLE IF NOT EXISTS `habitacion` (
  `idhabitacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '0',
  `descripcion` text NOT NULL,
  `capacidad` int(11) NOT NULL DEFAULT '1',
  `cantidad` int(11) DEFAULT '1',
  `imagenes` varchar(200) DEFAULT '["habitacion.jpg"]',
  `precio` float NOT NULL DEFAULT '0',
  `state` tinyint(4) DEFAULT '1' COMMENT '1 libre, 0 ocupada',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idhabitacion`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.habitacion: ~5 rows (approximately)
/*!40000 ALTER TABLE `habitacion` DISABLE KEYS */;
INSERT INTO `habitacion` (`idhabitacion`, `nombre`, `descripcion`, `capacidad`, `cantidad`, `imagenes`, `precio`, `state`, `created_at`, `updated_at`) VALUES
	(3, 'Matrimonial', '3 camas', 5, 1, '["habitacion.jpg"]', 12.56, 1, '0000-00-00 00:00:00', '2018-05-02 16:04:38'),
	(7, 'Doble', 'TV, radio', 1, 1, '["habitacion.jpg"]', 12, 1, '2018-03-08 17:30:28', '2018-05-01 03:34:35'),
	(8, 'Matrimonial y cama doble', 'TV', 1, 1, '["habitacion.jpg"]', 32, 1, '2018-03-08 17:31:24', '2018-05-01 03:10:46'),
	(9, 'Simple con jacussi', 'TV, radio', 6, 1, '["habitacion.jpg"]', 12, 1, '2018-03-08 17:33:43', '2018-05-01 02:01:10'),
	(10, 'Simple sin Bano', 'Con tv y cable', 5, 1, '["habitacion.jpg"]', 12.3, 1, '2018-03-09 04:50:02', '2018-03-09 04:50:02');
/*!40000 ALTER TABLE `habitacion` ENABLE KEYS */;

-- Dumping structure for table db_hotel.log
CREATE TABLE IF NOT EXISTS `log` (
  `idlog` int(11) NOT NULL AUTO_INCREMENT,
  `accion` int(11) DEFAULT NULL COMMENT '(0->aceptar solicitud , etc)',
  `idadministrador` int(11) DEFAULT NULL,
  `idcupon` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idlog`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.log: ~0 rows (approximately)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- Dumping structure for table db_hotel.registro
CREATE TABLE IF NOT EXISTS `registro` (
  `idregistro` int(11) NOT NULL AUTO_INCREMENT,
  `idbooking` int(11) DEFAULT '0',
  `idarticulo` int(11) NOT NULL DEFAULT '0',
  `cantidad` int(11) NOT NULL DEFAULT '0' COMMENT '0 salida, 1 entrada',
  `tipo` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idregistro`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table db_hotel.registro: ~17 rows (approximately)
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
INSERT INTO `registro` (`idregistro`, `idbooking`, `idarticulo`, `cantidad`, `tipo`, `created_at`) VALUES
	(1, 3, 2, 1, 0, '2018-05-01 22:15:22'),
	(2, 0, 12, 12, 1, '2018-05-01 23:13:41'),
	(3, 0, 13, 10, 1, '2018-05-01 23:14:33'),
	(4, 0, 13, 10, 1, '2018-05-01 23:31:26'),
	(5, 0, 13, 5, 1, '2018-05-01 23:31:38'),
	(6, 0, 13, 1, 1, '2018-05-01 23:34:15'),
	(7, 0, 13, 6, 0, '2018-05-01 23:34:43'),
	(8, 0, 3, 7, 0, '2018-05-01 23:35:58'),
	(9, 0, 2, 5, 0, '2018-05-01 23:36:41'),
	(10, 0, 3, 5, 0, '2018-05-01 23:36:42'),
	(11, 0, 13, 10, 0, '2018-05-01 23:36:42'),
	(12, 0, 2, 10, 0, '2018-05-01 23:40:47'),
	(13, 0, 3, 1, 0, '2018-05-01 23:40:47'),
	(14, 0, 13, 1, 0, '2018-05-01 23:40:47'),
	(15, 0, 2, 5, 0, '2018-05-01 23:41:40'),
	(16, 0, 2, 20, 1, '2018-05-02 11:42:13'),
	(17, 0, 2, 40, 0, '2018-05-02 11:43:52'),
	(18, 0, 2, 80, 1, '2018-05-02 11:49:16'),
	(19, 0, 2, 20, 1, '2018-05-02 13:26:25');
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;

-- Dumping structure for table db_hotel.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `idrol` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `state` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Dumping data for table db_hotel.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`idrol`, `tipo`, `nombre`, `state`) VALUES
	(1, 1, 'ADMINISTRADOR', 1),
	(2, 0, 'USUARIO', 1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
