@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Nombre: </th>
                                <th><h2>{{ $usuario->nombres }}</h2></th>
                            </tr>
                            <tr>
                                <th>Correo:</th>
                                <th><span class="alert alert-info">{{$usuario->email}}</span></th>
                            </tr>
                            <tr>
                                <th>Rol:</th>
                                <th><span class="alert alert-danger">{{$usuario->roles()->nombre}}</span></th>

                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
