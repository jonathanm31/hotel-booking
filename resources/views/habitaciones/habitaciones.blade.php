@extends('layouts.app')
{{--@section('titulo')--}}
{{--<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mensajes</span> - ultimos</h4>--}}
{{--@endsection--}}

@section('breadcrumb')
    <style type="text/css">
        .text-warning{
            font-weight: bold !important;
            color:#dea400 !important;
        }
        .text-success{
            font-weight: bold !important;
            color:#8fa316 !important;
        }
        .text-danger{
            font-weight: bold !important;
            color:#ca0812  !important;
        }
        .contenido {
            overflow: auto
        }
        .imagen-cupon {
            float: left
        }
        .imagen-cupon img {
            display: block
        }
        .titulo-cupon {
            margin-left: 120px;
        }
        .titulo-cupon h6{
            font-weight: bolder;
            color:#5b5c61;
        }
        .titulo-cupon span{
            color:#ca0812;
            font-weight: bold;
        }

        #ver:active{
            color:#fff !important;
        }
        nav li a:hover{
            color: #fff!important;
            background-color: #00BCD4 !important;
        }
        nav li .disabled:hover {
            color: inherit !important;
            background-color: inherit !important;
        }
        nav li a:active{
            color: #fff!important;
            background-color: #00BCD4 !important;
        }
        nav .pagination > .active > a{
            color: #fff!important;
            background-color: #00BCD4 !important;
            cursor:pointer;
        }
        nav li .active a #sig_btn:hover{
            color : #fff !important;
        }
        nav li a:active{
            color: #fff !important;
        }


    </style>

    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <i class="glyphicon glyphicon-phone-alt icon"></i>
            <a href="mensajes">Habitaciones</a>
        </li>
    </ul>
@endsection
@section('content')
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        <select id="tipo" type="text" class="form-control" name="tipo" placeholder="DNI COLABORADOR">
                            <option value="0">Todo</option>
                            <option value="1">Pendientes</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-offset-md-6">
                <div class="form-group text-right">
                    <div class="input-group">
                        <a href="nueva_habitacion" target="_top" class="btn btn-info"> Nueva Habitacion</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="table-responsive">
                <table id="table" class="table table-striped">
                    <thead>
                    <tr>
                        <th>Estado</th>
                        <th>Nombre</th>
                        <th>Descripion</th>
                        <th>Precio</th>
                        <th>Accion</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $habs = json_decode($habitaciones)->data->data;
                    ?>
                    @forelse($habs as $habitacion)
                        <tr>

                            <td>
                                <?PHP
                                switch ($habitacion->state){
                                    case '0':
                                        echo '<span class="text-warning">Ocupada</span><br>';
                                        echo '<b>desde el '. $habitacion->fec_ingreso .'</b><br>';
                                        echo '<b>hasta el '. $habitacion->fec_salida .'</b>';
                                        break;
                                    case '1':
                                        echo '<span class="text-success">Libre</span>';
                                        break;

                                }
                                ?>
                            </td>
                            <td>
                                <div class="contenido">
                                    <div class="imagen-cupon"> <img width="100" src="<?php
                                        $url = "http://localhost:8000/imghabitaciones/";
                                        $img = json_decode($habitacion->imagenes);
                                        echo $url.$img[0];
                                        ?>" class="img-responsive img-rounded" alt="">
                                    </div>
                                    <div class="titulo-cupon">
                                        <h6>{{ $habitacion->nombre}}</h6>
                                        <span>
                                           {{ $habitacion->capacidad }} Persona(s)
                                        </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <?php
                                $sub = substr($habitacion->descripcion,0,50)." ...";
                                echo $sub;
                                ?>
                            </td>
                            <td>
                               S/. {{ $habitacion->precio }}
                            </td>
                            <td>
                                <div class="boton-editar">
                                    <button class="btn btn-info text-slate-800 btn-flat" id="ver" data-id="{{$habitacion->idhabitacion}}" >
                                    <span style="color:#009bdd">
                                        Ver
                                    </span>
                                    </button>
                                    <button class="btn btn-danger text-slate-800 btn-flat" id="eliminar" data-id="{{$habitacion->idhabitacion}}" >
                                    <span style="color:#a2160a; font-weight: bolder; text-transform: uppercase">
                                        Eliminar
                                    </span>
                                    </button>
                                    <?PHP
                                        if($habitacion->state == '0'){
                                           ?>
                                    <button class="btn btn-success text-slate-800 btn-flat" id="liberar" data-id="{{$habitacion->idhabitacion}}" >
                                    <span style="color:#26a21a; font-weight: bolder; text-transform: uppercase">
                                        Liberar
                                    </span>
                                    </button>
                                    <?php
                                        }
                                    ?>

                                </div>
                            </td>
                        </tr>
                        <div id="ultimo" data-ult="0"></div>
                    @empty
                        <div id="ultimo" data-ult="1"> No tienes habitaciones.</div>
                    @endforelse




                    </tbody>
                </table>
            </div>
        </div>
        <div class="row" style="padding: 10px 20px">
            <div class="pull-right">
                <nav aria-label="Page navigation">
                    <ul class="pagination pagination-lg">
                        <li>
                            <a id="ant" class="" aria-label="Previous">
                                <span aria-hidden="true">&laquo; Anterior</span>
                            </a>
                        </li>
                        <li>
                            <a  id="sig" class=""  aria-label="Next">
                                <span id="sig_btn" class="" aria-hidden="true">Siguiente &raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

    </div>

    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        var urlParams = new URLSearchParams(window.location.search);
        var pag_actual = urlParams.get('pag');
        var id = parseInt(pag_actual);
        $(' #ver' ).click(function(){
            var idhabitacion = $(this).data('id');
            window.location.href = currentLocation+'habitacion?id='+idhabitacion;
        });
        $(' #eliminar' ).click(function(){
            var idhabitacion = $(this).data('id');
            swal({
                title: "¿Está seguro que deseas eliminar esta habitacion?",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "CONFIRMAR",
                cancelButtonText: "Salir",
                closeOnConfirm: false,
                closeOnCancel: false
            },function(isConfirm) {
                if (isConfirm) {
                    $.post(currentLocation+'eliminarhabitacion',{idhabitacion: idhabitacion}, function(data){
                        var resp = JSON.parse(data);
                        if(resp.status === '100'){
                            swal("Upps!", "Intentalo luego nuevamente!");
                        }else{
                            swal({title: "Listo", text: "Se ha cancelado exitosamente."},
                                function(){
                                    location.reload();
                                }
                            );
                        }
                    });
                    location.reload();

                } else {
                    swal("Ok, saliste!");

                }
            });
            $(".sweet-alert").css('background-color', '#a2160a').css('color','#fff');
        });
        $(' #liberar' ).click(function(){
            var idhabitacion = $(this).data('id');
            swal({
                title: "¿Está seguro que esta habitacion no esta ocupada?",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "CONFIRMAR",
                cancelButtonText: "Salir",
                closeOnConfirm: false,
                closeOnCancel: false
            },function(isConfirm) {
                if (isConfirm) {
                    $.post(currentLocation+'liberar',{idhabitacion: idhabitacion}, function(data){
                        var resp = JSON.parse(data);
                        if(resp.status === '100'){
                            swal("Upps!", "Intentalo luego nuevamente!");
                        }else{
                            swal({title: "Listo", text: "Se ha liberado exitosamente."},
                                function(){
                                   // location.reload();
                                }
                            );
                        }
                    });
                   // location.reload();

                } else {
                    swal("Ok, saliste!");

                }
            });
            $(".sweet-alert").css('background-color', '#26a21a').css('color','#fff');
        });

    </script>
@endsection
