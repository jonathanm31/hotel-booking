@extends('layouts.app')
{{--@section('titulo')--}}
{{--<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mensaje</span></h4>--}}
{{--@endsection--}}

@section('breadcrumb')
    <style type="text/css">
        .text-warning{
            font-weight: bold !important;
            color:#dea400 !important;
        }
        .text-success{
            font-weight: bold !important;
            color:#8fa316 !important;
        }
        .text-danger{
            font-weight: bold !important;
            color:#ca0812  !important;
        }
        .bg-danger{
            background-color:#5b5c61 !important;
            border: #5b5c61 !important;
        }
        .bg-success{
            background-color: #8fa316 !important;
            border: #8fa316 !important;
        }
        .confirm{
            background-color: #FFF !important;
            color:#000 !important;
        }
        .cancel{
            color:#d2d2d2 !important;
        }
        .sweet-alert input{
            color:#5b5c61 !important;
        }
        .row{
            margin: 5px;
        }
    </style>

    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <i class="glyphicon glyphicon glyphicon-list-alt icon"></i>
            <a href="booking">Inventario</a>
        </li>
    </ul>
@endsection
@section('content')
    <?php
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <h4>Movimientos</h4>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <select class="form-control" name="articulos" id="articulos">
                                        <option value="0" selected disabled>Escoge un  Producto y dale click a 'Agregar'</option>
                                        @forelse($articulos->data as $articulo)
                                            <option value="{{$articulo->idarticulo}}">{{$articulo->nombre}}</option>
                                        @empty
                                            <option value="0" selected disabled>No hay articulos </option>
                                        @endforelse
                                    </select>
                                    <span class="input-group-btn">
                                    <button class="btn btn-success" id="agregar"><i class="glyphicon glyphicon-upload"></i> Agregar</button>
                                  </span>
                                </div><!-- /input-group -->
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <div class="input-group">
                                    <div class='radio'><label><input  type='radio' name='optradio' value='0' id='tipo'>Salida</label></div>
                                    <div class='radio'><label><input type='radio' name='optradio' value='1' id='tipo'>Entrada</label></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="table-responsive">
                                    <table id="table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Cantidad Actual</th>
                                            <th>Cantidad</th>
                                            <th>Tipo</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody id="table">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-12 text-right">
                                <button type="button" id="guardar" data-id="" class="btn bg-success"><i class="icon-checkmark position-left"></i>Guardar</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6>Movimientos desde {{$inventario->from}} al {{ $inventario->to }}</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Tipo de Movimiento</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                            <th>Fecha</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($inventario->data as $producto)
                                            <tr>
                                                <td>
                                                    {{$producto->nombre}}
                                                </td>
                                                <td>
                                                    @if($producto->tipo == 0)
                                                        Salida
                                                        @else
                                                        Entrada
                                                    @endif
                                                </td>
                                                <td>
                                                    {{$producto->cantidad}}
                                                </td>
                                                <td>
                                                    {{$producto->precio}}
                                                </td>
                                                <td>
                                                    {{$producto->created_at}}
                                                </td>
                                            </tr>
                                        @empty
                                            <span class="text-warning">No tiene productos en inventario este dia.</span>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        var listaArticulos = JSON.parse(' <?php echo json_encode($articulos->data);  ?> ');

        $('#agregar').click(function(){
            var id = $('#articulos').val();
            var articulo;
            for(var i = 0 ; i < listaArticulos.length ; i++ ){
                if(listaArticulos[i].idarticulo == id){
                    articulos = listaArticulos[i];
                    break;
                }
            }
            var string = "<tr data-id='"+ articulos.idarticulo +"'><td>"+articulos.nombre+"</td><td>"+articulos.cantidad+"</td><td><input type='number' value='1' id='cantidad' class='form-control'></td><td><button class='btn btn-danger' id='quitar' data-id='"+ articulos.idarticulo +"'>Quitar </button></tr></td>";
            $("#table").append(string);
        });
        $("#table").on('click','td #quitar',function(){
            var tr_hab = $(this).parent().parent();
            tr_hab.remove();
        })
        $('#guardar').click(function(){
            var producto = [];
            var tipo = $('input[name=optradio]:checked').val();
            $('#table > tr').each(function() {
                producto.push({'idproducto': $(this).data('id'), 'tipo' : tipo , 'cantidad' : $(this).find('td #cantidad').val()});
            });
            var data = {'productos': JSON.stringify(producto)};
            $.post(currentLocation+"movimiento",data,function(data){
                var response = JSON.parse(data);
                window.location.href = currentLocation+"inventario";
            });
        });
    </script>
@endsection