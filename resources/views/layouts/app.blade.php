<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">
    <title>Sistema HOTEL</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>

    <script src="{{ asset('assets/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('assets/js/loadingoverlay_progress.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/lista.css') }}" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


    <!-- /core JS files -->


    <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/color/spectrum.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->


</head>
<body class="sidebar-xs has-detached-left">

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
        <a class="navbar-brand" href="/">
            {{--<img src="{{asset('images/Linicio.png')}}">--}}
            SISTEMA HOTEL
        </a>


    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">


        <ul class="nav navbar-nav navbar-right">
            <li class="">
                <a class="dropdown-toggle"href="logout">
                    <i class="icon-switch2"></i> Salir</a>

                {{--<ul class="dropdown-menu dropdown-menu-right">--}}
                    {{--<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>--}}
                    {{--<li><a href="#"><i class="icon-coins"></i> My balance</a></li>--}}
                    {{--<li><a href="#"><span class="badge badge-warning pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>--}}
                    {{--<li class="divider"></li>--}}
                    {{--<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>--}}
                    {{--<li><a href="/logout"><i class="icon-switch2"></i> Logout</a></li>--}}
                {{--</ul>--}}
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                {{--<div class="sidebar-user">--}}
                    {{--<div class="category-content">--}}
                        {{--<div class="media">--}}
                            {{--<a href="#" class="media-left"><img src="assets/images/image.png" class="img-circle img-sm" alt=""></a>--}}
                            {{--<div class="media-body">--}}
                                {{--<span class="media-heading text-semibold">Victoria Baker</span>--}}
                                {{--<div class="text-size-mini text-muted">--}}
                                    {{--<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="media-right media-middle">--}}
                                {{--<ul class="icons-list">--}}
                                    {{--<li>--}}
                                        {{--<a href="#"><i class="icon-cog3"></i></a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            {{--<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>--}}
                            @if (!Auth::guest())
                                <li class="{{ Request::is('/') ? 'class="start active open nav-item start"' : '' }}">
                                    <a href="{{ url('/') }}" class="nav-link nav-toggle">
                                        <img width="20" src="{{ Request::is('/') ? asset('images/icons/stats.png') : asset('images/icons/stats1.png') }}" alt="">
                                        <span class="title">Panel</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('/bookings') ? 'class="start active open nav-item start"' : '' }}">
                                    <a href="{{ url('/bookings') }}" class="nav-link nav-toggle">
                                        <i class="glyphicon glyphicon glyphicon-list-alt"></i>
                                        <span class="title"> Bookings</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('/habitaciones') ? 'class="start active open nav-item start"' : '' }}">
                                    <a href="{{ url('/habitaciones') }}" class="nav-link nav-toggle">
                                        <i class="glyphicon glyphicon-phone-alt"></i>
                                        <span class="title"> Habitaciones</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('/articulos') ? 'class="start active open nav-item start"' : '' }}">
                                    <a href="{{ url('/articulos') }}" class="nav-link nav-toggle">
                                        <i class="glyphicon glyphicon glyphicon-barcode"></i>
                                        <span class="title"> Productos</span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('/inventario') ? 'class="start active open nav-item start"' : '' }}">
                                    <a href="{{ url('/inventario') }}" class="nav-link nav-toggle">
                                        <i class="glyphicon glyphicon glyphicon-transfer"></i>
                                        <span class="title"> Inventario</span>
                                    </a>
                                </li>
                                @if(Auth::user()->idrol == '1')
                                    {{--<li class="{{ Request::is('cupones') ? 'class="start active open nav-item start"' : '' }}">--}}
                                        {{--<a href="{{ url('/cupones') }}" class="nav-link nav-toggle">--}}
                                            {{--<img width="20" src="{{ Request::is('cupones') ? asset('images/icons/cupon.png') : asset('images/icons/cupon1.png') }}" alt="">--}}
                                            {{--<span class="title">Cupones</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="{{ Request::is('mensajes') ? 'class="start active open nav-item start"' : '' }}">--}}
                                        {{--<a href="{{ url('/mensajes') }}" class="nav-link nav-toggle">--}}
                                            {{--<img width="20" src="{{ Request::is('mensajes') ? asset('images/icons/mensaje.png') : asset('images/icons/mensaje1.png') }}" alt="">--}}
                                            {{--<span class="title">Mensajes</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="{{ Request::is('colaboradores') ? 'class="start active open nav-item start"' : '' }}">--}}
                                        {{--<a href="{{ url('/colaboradores') }}" class="nav-link nav-toggle">--}}
                                            {{--<img width="20" src="{{ Request::is('colaboradores') ? asset('images/icons/colaboradores.png') : asset('images/icons/colaboradores1.png') }}" alt="">--}}
                                            {{--<span class="title">Colaboradores</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}


                                @endif

                                {{--@if(Auth::user()->idrol == '1' || Auth::user()->idrol == '0' )--}}
                                    {{--<li class="">--}}
                                        {{--<a href="{{ url('/user_panel') }}" class="nav-link nav-toggle">--}}
                                            {{--<i class="icon-diamond"></i>--}}
                                            {{--<span class="title">Usuario</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                            {{--@endif--}}


                        @endif
                       </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    {{--<div class="page-title">--}}
                        {{--@if (!Auth::guest())--}}
                            {{--@yield('titulo')--}}
                        {{--@endif--}}

                    {{--</div>--}}

                    <div class="heading-elements">
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        @if (!Auth::guest())
                            @yield('breadcrumb')
                        @endif

                    </ul>

                    <ul class="breadcrumb-elements">

                        @if (!Auth::guest())
                            @yield('menu')
                        @endif

                        <li><a href="#"><i class="icon-comment-discussion position-left"></i> Ayuda</a></li>

                    </ul>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Detached content -->
                <div class="container-detached">

                    @if (!Auth::guest())
                        @yield('content')
                    @endif

                </div>
                <!-- /detached content -->


                <!-- Footer -->
                <div class="footer text-muted">
                    2017 <a href="#"></a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
<script type="application/javascript">
    $(document).ajaxSend(function(){
        $.LoadingOverlay("show", {
            fade  : [2000, 1000],
            zIndex          : 1080
        });
    });
    $(document).ajaxComplete(function(){
        $.LoadingOverlay("hide");
    });
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
</script>
<script src="{{ asset('javascript/jquery.redirect.js') }}"></script>

</body>
</html>

