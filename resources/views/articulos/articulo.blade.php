<?php
$art = $articulo;
?>
@extends('layouts.app')
{{--@section('titulo')--}}
{{--<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mensaje</span></h4>--}}
{{--@endsection--}}

@section('breadcrumb')
    <link rel="stylesheet" href="{{ asset('css/galeria.css') }}">
    <!-- bootstrap 4.x is supported. You can also use the bootstrap css 3.3.x versions -->
    <link href="{{ asset('assets/fileloader/css/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/fileloader/css/gly/theme.css') }}" media="all" rel="stylesheet" type="text/css" />

    <!-- if using RTL (Right-To-Left) orientation, load the RTL CSS file after fileinput.css by uncommenting below -->
    <!-- link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput-rtl.min.css" media="all" rel="stylesheet" type="text/css" /-->
    <!-- piexif.min.js is only needed for restoring exif data in resized images and when you
        wish to resize images before upload. This must be loaded before fileinput.min.js -->
    <script src="{{ asset('assets/fileloader/js/plugins/piexif.min.js')}}" type="text/javascript"></script>
    <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
        This must be loaded before fileinput.min.js -->
    <script src="{{ asset('assets/fileloader/js/plugins/sortable.min.js')}}" type="text/javascript"></script>
    <!-- purify.min.js is only needed if you wish to purify HTML content in your preview for
        HTML files. This must be loaded before fileinput.min.js -->
    <script src="{{ asset('assets/fileloader/js/plugins/purify.min.js')}}" type="text/javascript"></script>
    <!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js
       3.3.x versions without popper.min.js. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <!-- bootstrap.min.js below is needed if you wish to zoom and preview file content in a detail modal
        dialog. bootstrap 4.x is supported. You can also use the bootstrap js 3.3.x versions. -->
    <!-- the main fileinput plugin file -->
    <script src="{{ asset('assets/fileloader/js/fileinput.min.js')}}"></script>
    <!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
    <script src="{{ asset('assets/fileloader/themes/gly/theme.js')}}"></script>
    <!-- optionally if you need translation for your language then include  locale file as mentioned below -->
    <script src="{{ asset('assets/fileloader/js/locales/fr.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>

    <style type="text/css">
        .text-warning{
            font-weight: bold !important;
            color:#dea400 !important;
        }
        .text-success{
            font-weight: bold !important;
            color:#8fa316 !important;
        }
        .text-danger{
            font-weight: bold !important;
            color:#ca0812  !important;
        }
        .bg-danger{
            background-color:#5b5c61 !important;
            border: #5b5c61 !important;
        }
        .bg-success{
            background-color: #8fa316 !important;
            border: #8fa316 !important;
        }
        .confirm{
            background-color: #FFF !important;
            color:#000 !important;
        }
        .cancel{
            color:#d2d2d2 !important;
        }
        .sweet-alert input{
            color:#5b5c61 !important;
        }
        .row{
            margin: 5px;
        }
    </style>

    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <i class="glyphicon glyphicon-phone-alt icon"></i>
            <a href="articulos">Productos</a>
        </li>
        <li>Producto</li>
    </ul>
@endsection
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Datos del Producto:</legend>
                                    <div class="row form-group">
                                        <label for="nombre">Producto:</label>
                                        <input type="text" id="nombre" class="form-control" value="{{ $art->nombre }}" >
                                        <input type="hidden" id="idarticulo" value="{{ $art->idarticulo  }}">
                                    </div>
                                <div class="row form-group">
                                        <label for="cantidad">Cantidad:</label>
                                        <input type="text" id="cantidad" class="form-control" value="{{ $art->cantidad }}" disabled>
                                      <i>/u</i>
                                </div>
                                <div class="row form-group">
                                        <label for="precio">Precio:</label>
                                        <input type="text" id="precio" class="form-control" value="{{ $art->precio }}" >
                               </div>
                                    <div class="row form-group">
                                        <label for="costo">Costo:</label>
                                        <input type="text" id="costo" class="form-control" value="{{ $art->costo }}" >
                                    </div>

                                </fieldset>
                            </div>
                            <div class="col-md-12 text-right">
                                    <div class="row form-group">
                                        <textarea type="text" id="descripcion" class="wysihtml5 wysihtml5-min form-control" rows="10" >
                                            {{$art->descripcion}}
                                        </textarea>
                                    </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-12 text-right">
                                <?PHP
                                switch ($art->state)    {
                                case '0':
                                ?>
                                <button type="button" id="denegar" data-id="{{$art->idarticulo}}" class="btn bg-danger"><i class="icon-cross2 position-left"></i>Denegar</button>
                                <button type="button" id="aprobar" data-id="{{$art->idarticulo}}" class="btn bg-success"><i class="icon-checkmark position-left"></i>Aprobar</button>
                                <?php
                                break;
                                case '1':
                                ?>
                                <button type="button" id="cancelar" data-id="{{$art->idarticulo}}" class="btn bg-danger"><i class="icon-cross2 position-left"></i>Cancelar</button>
                                <button type="button" id="guardar" data-id="{{$art->idarticulo}}" class="btn bg-success"><i class="icon-checkmark position-left"></i>Guardar</button>
                                <?php
                                break;
                                case '2':
                                    break;
                                case '3':
                                    break;
                                }
                                ?>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        $("#input-fa-1").fileinput({
            theme: "gly",
            uploadUrl: 'get/img'
        });
        $('.wysihtml5').wysihtml5({
            parserRules:  wysihtml5ParserRules,
            stylesheets: ["assets/css/components.css"],
            "image": false,
            "link": false,
            "font-styles": false,
            "emphasis": false
        });

        $('#cancelar').click(function(){
            window.location.href = "/articulos";
        });
        $('#guardar').click(function(){
           var idarticulo = $('#idarticulo').val();
           var nombre = $('#nombre').val();

           var precio = $('#precio').val();
           var costo = $('#costo').val();
           var descripcion = $('#descripcion').val();
           var estado = 1;

            var data = {"idarticulo": idarticulo, "nombre": nombre, "precio" :precio,"costo" :costo, "descripcion":descripcion, "state":1 };
            var url = currentLocation +"editararticulo";
            $.post(url,data,function(respuesta){
                var articulo = JSON.parse(respuesta);
                window.location.href = currentLocation+"articulos?id="+ articulo.data.idarticulo;
            });
        });
    </script>
@endsection