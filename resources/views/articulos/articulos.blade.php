@extends('layouts.app')
{{--@section('titulo')--}}
{{--<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mensajes</span> - ultimos</h4>--}}
{{--@endsection--}}

@section('breadcrumb')
    <style type="text/css">
        .text-warning{
            font-weight: bold !important;
            color:#dea400 !important;
        }
        .text-success{
            font-weight: bold !important;
            color:#8fa316 !important;
        }
        .text-danger{
            font-weight: bold !important;
            color:#ca0812  !important;
        }
        .contenido {
            overflow: auto
        }
        .imagen-cupon {
            float: left
        }
        .imagen-cupon img {
            display: block
        }
        .titulo-cupon {
            margin-left: 120px;
        }
        .titulo-cupon h6{
            font-weight: bolder;
            color:#5b5c61;
        }
        .titulo-cupon span{
            color:#ca0812;
            font-weight: bold;
        }

        #ver:active{
            color:#fff !important;
        }
        nav li a:hover{
            color: #fff!important;
            background-color: #00BCD4 !important;
        }
        nav li .disabled:hover {
            color: inherit !important;
            background-color: inherit !important;
        }
        nav li a:active{
            color: #fff!important;
            background-color: #00BCD4 !important;
        }
        nav .pagination > .active > a{
            color: #fff!important;
            background-color: #00BCD4 !important;
            cursor:pointer;
        }
        nav li .active a #sig_btn:hover{
            color : #fff !important;
        }
        nav li a:active{
            color: #fff !important;
        }


    </style>

    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <i class="glyphicon glyphicon glyphicon-barcode icon"></i>
            <a href="mensajes">Productos</a>
        </li>
    </ul>
@endsection
@section('content')
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        <select id="tipo" type="text" class="form-control" name="tipo" placeholder="DNI COLABORADOR">
                            <option value="0">Todo</option>
                            <option value="1">Pendientes</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-offset-md-6">
                <div class="form-group text-right">
                    <div class="input-group">
                        <a href="nuevo_articulo" target="_top" class="btn btn-info"> Nuevo Articulo</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="table-responsive">
                <table id="table" class="table table-striped">
                    <thead>
                    <tr>
                        <th>Estado</th>
                        <th>Nombre</th>
                        <th>Descripion</th>
                        <th>Precio</th>
                        <th>Costo</th>
                        <th>Accion</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                            try{
                                $arts = json_decode($articulos)->data->data;
                            }catch (Exception $exception){
                                $arts = [];
                            }
                    ?>

                    @forelse($arts as $articulo)
                        <tr>

                            <td>
                                <?PHP
                                switch ($articulo->state){
                                    case '0':
                                        echo '<span class="text-warning">No hay</span>';
                                        break;
                                    case '1':
                                        echo '<span class="text-success">En Stock</span>';
                                        break;

                                }
                                ?>
                            </td>
                            <td>
                                <div class="contenido">
                                    <div>
                                        <h6>{{ $articulo->nombre}}</h6>
                                        <span>
                                           {{ $articulo->cantidad }} unidades(s)
                                        </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <?php
                                $sub = substr($articulo->descripcion,0,50)." ...";
                                echo $sub;
                                ?>
                            </td>
                            <td>
                                S/. {{ $articulo->precio }}
                            </td>
                            <td>
                                S/. {{ $articulo->costo }}
                            </td>
                            <td>
                                <div class="boton-editar">
                                    <button class="btn btn-info text-slate-800 btn-flat" id="ver" data-id="{{$articulo->idarticulo}}" >
                                    <span style="color:#009bdd">
                                        Ver
                                    </span>
                                    </button>
                                    <button class="btn btn-danger text-slate-800 btn-flat" id="eliminar" data-id="{{$articulo->idarticulo}}" >
                                    <span style="color:#a2160a; font-weight: bolder; text-transform: uppercase">
                                        Eliminar
                                    </span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <div id="ultimo" data-ult="0"></div>
                    @empty
                        <div id="ultimo" data-ult="1"> No tienes Productos.</div>
                    @endforelse




                    </tbody>

                </table>
            </div>
        </div>
        <div class="row" style="padding: 10px 20px">
            <div class="pull-right">
                <nav aria-label="Page navigation">
                    <ul class="pagination pagination-lg">
                        <li>
                            <a id="ant" class="" aria-label="Previous">
                                <span aria-hidden="true">&laquo; Anterior</span>
                            </a>
                        </li>
                        <li>
                            <a  id="sig" class=""  aria-label="Next">
                                <span id="sig_btn" class="" aria-hidden="true">Siguiente &raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

    </div>

    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        var urlParams = new URLSearchParams(window.location.search);
        var pag_actual = urlParams.get('pag');
        var id = parseInt(pag_actual);
        $(' #ver' ).click(function(){
            var idarticulo = $(this).data('id');
            window.location.href = currentLocation+'articulo?id='+idarticulo;
        });
        $(' #eliminar' ).click(function(){
            var idarticulo = $(this).data('id');
            swal({
                title: "¿Está seguro que deseas eliminar este producto?",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "CONFIRMAR",
                cancelButtonText: "Salir",
                closeOnConfirm: false,
                closeOnCancel: false
            },function(isConfirm) {
                if (isConfirm) {
                    $.post(currentLocation+'eliminararticulo',{idarticulo: idarticulo}, function(data){
                        var resp = JSON.parse(data);
                        if(resp.status === '100'){
                            swal("Upps!", "Intentalo luego nuevamente!");
                        }else{
                            swal({title: "Listo", text: resp.mensaje},
                                function(){
                                    location.reload();
                                }
                            );
                        }
                    });
                   location.reload();

                } else {
                    swal("Ok, saliste!");

                }
            });
            $(".sweet-alert").css('background-color', '#a2160a').css('color','#fff');
        });

    </script>
@endsection
