@extends('layouts.app')
{{--@section('titulo')--}}
{{--<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mensaje</span></h4>--}}
{{--@endsection--}}

@section('breadcrumb')
    <link rel="stylesheet" href="{{ asset('css/galeria.css') }}">
    <!-- bootstrap 4.x is supported. You can also use the bootstrap css 3.3.x versions -->
    <link href="{{ asset('assets/fileloader/css/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/fileloader/css/gly/theme.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/js/datapicker/bootstrap-datetimepicker.css') }}">

    <!-- if using RTL (Right-To-Left) orientation, load the RTL CSS file after fileinput.css by uncommenting below -->
    <!-- link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput-rtl.min.css" media="all" rel="stylesheet" type="text/css" /-->
    <!-- piexif.min.js is only needed for restoring exif data in resized images and when you
        wish to resize images before upload. This must be loaded before fileinput.min.js -->
    <script src="{{ asset('assets/fileloader/js/plugins/piexif.min.js')}}" type="text/javascript"></script>
    <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
        This must be loaded before fileinput.min.js -->
    <script src="{{ asset('assets/fileloader/js/plugins/sortable.min.js')}}" type="text/javascript"></script>
    <!-- purify.min.js is only needed if you wish to purify HTML content in your preview for
        HTML files. This must be loaded before fileinput.min.js -->
    <script src="{{ asset('assets/fileloader/js/plugins/purify.min.js')}}" type="text/javascript"></script>
    <!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js
       3.3.x versions without popper.min.js. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <!-- bootstrap.min.js below is needed if you wish to zoom and preview file content in a detail modal
        dialog. bootstrap 4.x is supported. You can also use the bootstrap js 3.3.x versions. -->
    <!-- the main fileinput plugin file -->
    <script src="{{ asset('assets/fileloader/js/fileinput.min.js')}}"></script>
    <!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
    <script src="{{ asset('assets/fileloader/themes/gly/theme.js')}}"></script>
    <!-- optionally if you need translation for your language then include  locale file as mentioned below -->
    <script src="{{ asset('assets/fileloader/js/locales/fr.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/datapicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/datapicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>

    <style type="text/css">
        .text-warning{
            font-weight: bold !important;
            color:#dea400 !important;
        }
        .text-success{
            font-weight: bold !important;
            color:#8fa316 !important;
        }
        .text-danger{
            font-weight: bold !important;
            color:#ca0812  !important;
        }
        .bg-danger{
            background-color:#5b5c61 !important;
            border: #5b5c61 !important;
        }
        .bg-success{
            background-color: #8fa316 !important;
            border: #8fa316 !important;
        }
        .confirm{
            background-color: #FFF !important;
            color:#000 !important;
        }
        .cancel{
            color:#d2d2d2 !important;
        }
        .sweet-alert input{
            color:#5b5c61 !important;
        }
        .row{
            margin: 5px;
        }
    </style>

    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <i class="glyphicon glyphicon glyphicon-list-alt icon"></i>
            <a href="booking">Booking</a>
        </li>
    </ul>
@endsection
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row"> <fieldset>
                            <div class="col-md-6">

                                    <legend>Datos del Cliente:</legend>
                                    <div class="row form-group">
                                        <label for="dni">DNI:</label>
                                        <input type="text" id="dni" class="form-control" value="" >
                                    </div>
                                    <div class="row form-group">
                                        <label for="nombres">Nombres:</label>
                                        <input type="text" id="nombres" class="form-control" value="" >
                                    </div>
                                    <div class="row form-group">
                                        <label for="apellidos">Apellidos:</label>
                                        <input type="text" id="apellidos" class="form-control" value="" >
                                    </div>

                                    <div class="row form-group">
                                        <label for="ruc">RUC:</label>
                                        <input type="text" id="ruc" class="form-control" value="" >
                                    </div>
                                    <div class="row form-group">
                                        <label for="email">Email:</label>
                                        <input type="text" id="email" class="form-control" value="" >
                                    </div>
                                    <div class="row form-group">
                                        <label for="telefono">Telefono:</label>
                                        <input type="text" id="telefono" class="form-control" value="" >
                                    </div>
                                    <div class="row form-group">
                                        <label for="direccion">Direccion:</label>
                                        <input type="text" id="direccion" class="form-control" value="" >
                                    </div>


                            </div>
                            <div class="col-md-6 text-right">
                                <div class="row form-group">
                                    <label for="adultos">Cant. Adultos:</label>
                                    <input type="number" id="adultos" class="form-control" value="" >
                                </div>
                                <div class="row form-group">
                                    <label for="ninos">Cant. Niños:</label>
                                    <input type="number" id="ninos" class="form-control" value="" >
                                </div>
                                <div class="row form-group"> <label for="ingreso">Fecha de Ingreso:</label>
                                    <div class='input-group date' id='fecha1'>
                                        <input type='text' id="ingreso" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="row form-group"><label for="salida">Fecha de Salida:</label>
                                    <div class='input-group date' id='fecha2'>

                                        <input type='text' id="salida" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="costo">Descripcion:</label>
                                        <textarea type="text" id="descripcion" class="wysihtml5 wysihtml5-min form-control" rows="10" >

                                        </textarea>
                                </div>
                            </div>
                            </fieldset>
                        </div>
                        <?php $habs = json_decode($habitaciones)->data;
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <select class="form-control" name="habitaciones" id="habitaciones">
                                        <option value="0" selected disabled>Escoger habitacion y dale click a 'Agregar'</option>
                                        @forelse($habs as $habitacion)
                                            <option value="{{$habitacion->idhabitacion}}">{{$habitacion->nombre}}</option>
                                        @empty
                                            <option value="0" selected disabled>No hay habitaciones Libres</option>
                                        @endforelse
                                    </select>
                                    <span class="input-group-btn">
                                    <button class="btn btn-success" id="agregar"><i class="glyphicon glyphicon-upload"></i> Agregar</button>
                                  </span>
                                </div><!-- /input-group -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="table-responsive">
                                    <table id="table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Habitacion</th>
                                            <th>Capacidad</th>
                                            <th>Precio</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody id="table">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-12 text-right">

                                <button type="button" id="cancelar" data-id="" class="btn bg-danger"><i class="icon-cross2 position-left"></i>Cancelar</button>
                                <button type="button" id="guardar" data-id="" class="btn bg-success"><i class="icon-checkmark position-left"></i>Siguiente</button>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        $("#input-fa-1").fileinput({
            theme: "gly",
            uploadUrl: 'get/img'
        });
        $('.wysihtml5').wysihtml5({
            parserRules:  wysihtml5ParserRules,
            stylesheets: ["assets/css/components.css"],
            "image": false,
            "link": false,
            "font-styles": false,
            "emphasis": false
        });
        $('#cancelar').click(function(){
            window.location.href = "/booking";
        });
        $('#fecha1').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss' // change format here
        });
        $('#fecha2').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'YYYY-MM-DD HH:mm:ss' // change format here
        });
        $("#fecha1").on("dp.change", function (e) {
            $('#fecha2').data("DateTimePicker").minDate(e.date);
        });
        $("#fecha2").on("dp.change", function (e) {
            $('#fecha1').data("DateTimePicker").maxDate(e.date);
        });

        var listaHabitaciones = JSON.parse(' <?php echo json_encode($habs);  ?> ');

        $('#agregar').click(function(){
            var id = $('#habitaciones').val();
            var habitacion;
            console.log(id);
            for(var i = 0 ; i < listaHabitaciones.length ; i++ ){
                if(listaHabitaciones[i].idhabitacion == id){
                    habitacion = listaHabitaciones[i];
                    break;
                }
            }
            var string = "<tr data-id='"+ habitacion.idhabitacion +"'><td>"+habitacion.nombre+"</td><td><input type='number' id='cantidad' value='"+habitacion.capacidad+"' class='form-control'></td><td><input type='number' id='precio' class='form-control'  value='"+habitacion.precio.toFixed(2)+"'><td><button class='btn btn-danger' id='quitar' data-id='"+ habitacion.idhabitacion +"'>Quitar </button></tr></td>";
            $("#table").append(string);
        });
        $("#table").on('click','td #quitar',function(){
            var tr_hab = $(this).parent().parent();
            tr_hab.remove();
        })

        $('#guardar').click(function(){
                if(!validar()){
                    $('#nombres').parent().addClass('has-error');
                    $('#apellidos').parent().addClass('has-error');
                    $('#telefono').parent().addClass('has-error');
                    $('#dni').parent().addClass('has-error');
                    $('#descripcion').parent().addClass('has-error');
                    $('#ingreso').parent().addClass('has-error');
                    $('#salida').parent().addClass('has-error');
                    $('#habitaciones').parent().addClass('has-error');
                    swal('Faltan Campos');
                    return;
                }

                var nombres = $('#nombres').val();
                var apellidos = $('#apellidos').val();
                var email = $('#email').val();
                var direccion = $('#direccion').val();
                var telefono = $('#telefono').val();
                var ninos = $('#ninos').val();
                var adultos = $('#adultos').val();
                var dni = $('#dni').val();
                var ruc = $('#ruc').val();
                var detalle = $('#descripcion').val();
                var fec_ingreso = $('#ingreso').val();
                var fec_salida = $('#salida').val();
                var habitaciones = [];
                $('#table > tr').each(function() {
                    habitaciones.push({'idhabitacion': $(this).data('id'), 'precio' : $(this).find('td #precio').val(), 'cantidad' : $(this).find('td #cantidad').val()});
                });
                var data = {'ninos':ninos,'adultos':adultos,'nombres':nombres,'apellidos':apellidos,'email':email,'direccion':direccion,'telefono':telefono,'dni':dni,'ruc':ruc,'detalle':detalle,'fec_ingreso':fec_ingreso,'fec_salida':fec_salida,'habitaciones': JSON.stringify(habitaciones),'state':1};

                console.log(habitaciones);
                $.post(currentLocation+"guardarbooking",data,function(data){
                    var response = JSON.parse(data);
                    window.location.href = currentLocation+"detalle?idbooking="+response.data.idbooking;
                });
        });

        function validar(){
            var nombres = $('#nombres').val();
            var apellidos = $('#apellidos').val();
            var telefono = $('#telefono').val();
            var dni = $('#dni').val();
            var detalle = $('#descripcion').val();
            var fec_ingreso = $('#ingreso').val();
            var fec_salida = $('#salida').val();
            var habitaciones = [];
            $('#table > tr').each(function() {
                habitaciones.push({'idhabitacion': $(this).data('id'), 'precio' : $(this).find('td #precio').val(), 'cantidad' : $(this).find('td #cantidad').val()});
            });
//            $('#nombres').parent().removeClass('has-error');
//            $('#apellidos').parent().removeClass('has-error');
//            $('#telefono').parent().removeClass('has-error');
//            $('#dni').parent().removeClass('has-error');
//            $('#descripcion').parent().removeClass('has-error');
//            $('#ingreso').parent().removeClass('has-error');
//            $('#salida').parent().removeClass('has-error');

            if( nombres && apellidos && telefono && dni && detalle  && fec_ingreso && fec_salida && typeof habitaciones !== 'undefined' && habitaciones.length > 0 ){

                return true;
            };
            return false;
        }

        $('#dni').change(function(){
           var dni = $(this).val();
           if(dni.length === 8 ){
               $.post(currentLocation+"buscarcliente",{'dni':dni},function(data){
                   var cliente = JSON.parse(data);
                   console.log(cliente);
                   if(cliente.data !== null){
                       $('#nombres').val(cliente.data.nombres);
                       $('#apellidos').val(cliente.data.apellidos);
                       $('#email').val(cliente.data.email);
                       $('#direccion').val(cliente.data.direccion);
                       $('#telefono').val(cliente.data.telefono);
                       $('#ruc').val(cliente.data.ruc);
                   }else{
                       $('#nombres').val('');
                       $('#apellidos').val('')
                       $('#email').val('')
                       $('#direccion').val('')
                       $('#telefono').val('')
                       $('#ruc').val('')
                   }

               })
           }
        });

    </script>
@endsection