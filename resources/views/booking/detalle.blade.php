@extends('layouts.app')
{{--@section('titulo')--}}
{{--<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mensaje</span></h4>--}}
{{--@endsection--}}

@section('breadcrumb')
    <style type="text/css">
        .text-warning{
            font-weight: bold !important;
            color:#dea400 !important;
        }
        .text-success{
            font-weight: bold !important;
            color:#8fa316 !important;
        }
        .text-danger{
            font-weight: bold !important;
            color:#ca0812  !important;
        }
        .bg-danger{
            background-color:#5b5c61 !important;
            border: #5b5c61 !important;
        }
        .bg-success{
            background-color: #8fa316 !important;
            border: #8fa316 !important;
        }
        .confirm{
            background-color: #FFF !important;
            color:#000 !important;
        }
        .cancel{
            color:#d2d2d2 !important;
        }
        .sweet-alert input{
            color:#5b5c61 !important;
        }
        .row{
            margin: 5px;
        }
    </style>

    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <i class="glyphicon glyphicon glyphicon-list-alt icon"></i>
            <a href="booking">Detalle del Registro</a>
        </li>
    </ul>
@endsection
@section('content')
    <?php
            ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h4>
                            CODE:{{ $booking->data->cabecera->code }}
                        </h4>
                        <input type="hidden" id="idbooking" value="{{ $booking->data->cabecera->idbooking }}">
                        <small>
                            FECHA:{{ $booking->data->cabecera->fec_ingreso }} al
                            {{ $booking->data->cabecera->fec_salida }}
                        </small>

                        <div class="row">
                            <div class="col-md-6">
                                <h6>CLIENTE:</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>DNI</th>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>RUC</th>
                                            <th>Email</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                {{ $booking->data->cabecera->dni }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->nombres }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->apellidos }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->ruc }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->email }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <h6>Detalle</h6>
                                <p>{{ $booking->data->cabecera->detalle }}</p>
                                <h6>Cantidad de Personas</h6>
                                ADULTOS: <span>{{ $booking->data->cabecera->adultos }}</span>
                                NIÑOS: <span>{{ $booking->data->cabecera->ninos }}</span>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6>HABITACIONES:</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Cant. Personas</th>
                                            <th>Precio</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($booking->data->detalle as $habitacion)
                                                <tr>
                                                    <td>
                                                        {{$habitacion->nombre}}
                                                    </td>
                                                    <td>
                                                        {{$habitacion->descripcion}}
                                                    </td>
                                                    <td>
                                                        {{$habitacion->cantidad_bk}}
                                                    </td>
                                                    <td>
                                                        {{ floatval($habitacion->precio_bk) }}
                                                    </td>
                                                </tr>
                                                @empty
                                                <span class="text-warning">No tiene habitaciones asignadas.</span>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6>PRODUCTOS:</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($booking->data->productos as $producto)
                                            <tr>
                                                <td>
                                                    {{$producto->nombre}}
                                                </td>
                                                <td>
                                                    {{$producto->descripcion}}
                                                </td>
                                                <td>
                                                    {{$producto->cantidad}}
                                                </td>
                                                <td>
                                                    {{$producto->precio}}
                                                </td>
                                            </tr>
                                        @empty
                                           <span class="text-warning">No tiene productos asignados.</span>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <h4>Agregar Productos</h4>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <select class="form-control" name="articulos" id="articulos">
                                        <option value="0" selected disabled>Escoge un  Producto y dale click a 'Agregar'</option>
                                        @forelse($articulos->data as $articulo)
                                            <option value="{{$articulo->idarticulo}}">{{$articulo->nombre}}</option>
                                        @empty
                                            <option value="0" selected disabled>No hay articulos </option>
                                        @endforelse
                                    </select>
                                    <span class="input-group-btn">
                                    <button class="btn btn-success" id="agregar"><i class="glyphicon glyphicon-upload"></i> Agregar</button>
                                  </span>
                                </div><!-- /input-group -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="table-responsive">
                                    <table id="table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody id="table">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-12 text-right">
                                <button type="button" id="guardar" data-id="" class="btn bg-success"><i class="icon-checkmark position-left"></i>Guardar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        var listaArticulos = JSON.parse(' <?php echo json_encode($articulos->data);  ?> ');

        $('#agregar').click(function(){
            var id = $('#articulos').val();
            var articulo;
            for(var i = 0 ; i < listaArticulos.length ; i++ ){
                if(listaArticulos[i].idarticulo == id){
                    articulos = listaArticulos[i];
                    break;
                }
            }
            var string = "<tr data-id='"+ articulos.idarticulo +"'><td>"+articulos.nombre+"</td><td><input  class='form-control' type='number' value='"+articulos.cantidad+"' id='cantidad'></td><td><input  class='form-control' type='number' value='"+articulos.precio.toFixed(2)+"' id='precio'><td><button class='btn btn-danger' id='quitar' data-id='"+ articulos.idarticulo +"'>Quitar </button></tr></td>";
            $("#table").append(string);
        });
        $("#table").on('click','td #quitar',function(){
            var tr_hab = $(this).parent().parent();
            tr_hab.remove();
        })
        $('#guardar').click(function(){
            var producto = [];
            var idbooking = $('#idbooking').val();
            $('#table > tr').each(function() {
                producto.push({'idproducto': $(this).data('id'), 'precio' : $(this).find('td #precio').val(), 'cantidad' : $(this).find('td #cantidad').val()});
            });
            var data = {'idbooking':idbooking,'productos': JSON.stringify(producto),'state':1};

            console.log(data);
            $.post(currentLocation+"editar_booking",data,function(data){
                var response = JSON.parse(data);
                window.location.href = currentLocation+"detalle?idbooking="+response.data.idbooking;
            });
        });
    </script>
@endsection