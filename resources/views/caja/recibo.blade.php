@extends('layouts.app')
{{--@section('titulo')--}}
{{--<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mensaje</span></h4>--}}
{{--@endsection--}}

@section('breadcrumb')
    <style type="text/css">
        .text-warning{
            font-weight: bold !important;
            color:#dea400 !important;
        }
        .text-success{
            font-weight: bold !important;
            color:#8fa316 !important;
        }
        .text-danger{
            font-weight: bold !important;
            color:#ca0812  !important;
        }
        .bg-danger{
            background-color:#5b5c61 !important;
            border: #5b5c61 !important;
        }
        .bg-success{
            background-color: #8fa316 !important;
            border: #8fa316 !important;
        }
        .confirm{
            background-color: #FFF !important;
            color:#000 !important;
        }
        .cancel{
            color:#d2d2d2 !important;
        }
        .sweet-alert input{
            color:#5b5c61 !important;
        }
        .row{
            margin: 5px;
        }
    </style>

    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <i class="glyphicon glyphicon glyphicon-list-alt icon"></i>
            <a href="booking">RECIBO</a>
        </li>
    </ul>
@endsection
@section('content')
    <?php
            ?>
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">

                        <h4>
                            Numeracion: 001 - {{ str_pad($booking->data->caja->numeracion, 6, '0', STR_PAD_LEFT) }}
                        </h4>
                        <input type="hidden" id="idbooking" value="{{ $booking->data->cabecera->idbooking }}">
                        <small>
                            FECHA:{{ $booking->data->caja->created_at }} <br>
                            TIPO: @if($booking->data->caja->tipo_boleta == 0)
                                      Boleta
                                  @else
                                      Factura
                                  @endif
                        </small>
                        <div class="row">
                            <div class="col-md-6">
                                <h6>CLIENTE:</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>DNI</th>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>RUC</th>
                                            <th>Email</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                {{ $booking->data->cabecera->dni }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->nombres }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->apellidos }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->ruc }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->email }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <h6>Detalle</h6>
                                <p>{{ $booking->data->cabecera->detalle }}</p>
                                <h6>Cantidad de Personas</h6>
                                ADULTOS: <span>{{ $booking->data->cabecera->adultos }}</span>
                                NIÑOS: <span>{{ $booking->data->cabecera->ninos }}</span>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h6>HABITACIONES:</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Capacidad</th>
                                            <th>Precio</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($booking->data->detalle as $habitacion)
                                                <tr>
                                                    <td>
                                                        {{$habitacion->nombre}}
                                                    </td>
                                                    <td>
                                                        {{$habitacion->descripcion}}
                                                    </td>
                                                    <td>
                                                        {{$habitacion->capacidad}}
                                                    </td>
                                                    <td>
                                                        {{$habitacion->precio}}
                                                    </td>
                                                </tr>
                                                @empty
                                                No tiene habitaciones asignadas.
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6>PRODUCTOS:</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($booking->data->productos as $producto)
                                            <tr>
                                                <td>
                                                    {{$producto->nombre}}
                                                </td>
                                                <td>
                                                    {{$producto->descripcion}}
                                                </td>
                                                <td>
                                                    {{$producto->cantidad}}
                                                </td>
                                                <td>
                                                    {{$producto->precio}}
                                                </td>
                                            </tr>
                                        @empty
                                           <span class="text-warning">No tiene productos asignados.</span>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsible">
                                    <table class="table">
                                        <TR>
                                            <th>
                                                Total sin IGV
                                            </th>
                                            <td>
                                                {{ $booking->data->caja->total -  $booking->data->caja->igv   }}
                                            </td>
                                        </TR>
                                        <TR>
                                            <th>
                                                IGV
                                            </th>
                                            <td>
                                                {{ $booking->data->caja->igv  }}
                                            </td>
                                        </TR>
                                        <TR>
                                            <th>
                                               Descuento
                                            </th>
                                            <td>
                                                {{ $booking->data->caja->descuento  }}
                                            </td>
                                        </TR>
                                        <TR>
                                            <th>
                                                Total
                                            </th>
                                            <td>
                                                {{ $booking->data->caja->total  }}
                                            </td>
                                        </TR>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection