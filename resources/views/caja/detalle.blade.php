@extends('layouts.app')
{{--@section('titulo')--}}
{{--<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mensaje</span></h4>--}}
{{--@endsection--}}

@section('breadcrumb')
    <style type="text/css">
        .text-warning{
            font-weight: bold !important;
            color:#dea400 !important;
        }
        .text-success{
            font-weight: bold !important;
            color:#8fa316 !important;
        }
        .text-danger{
            font-weight: bold !important;
            color:#ca0812  !important;
        }
        .bg-danger{
            background-color:#5b5c61 !important;
            border: #5b5c61 !important;
        }
        .bg-success{
            background-color: #8fa316 !important;
            border: #8fa316 !important;
        }
        .confirm{
            background-color: #FFF !important;
            color:#000 !important;
        }
        .cancel{
            color:#d2d2d2 !important;
        }
        .sweet-alert input{
            color:#5b5c61 !important;
        }
        .row{
            margin: 5px;
        }
    </style>

    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <i class="glyphicon glyphicon glyphicon-list-alt icon"></i>
            <a href="booking">Caja</a>
        </li>
    </ul>
@endsection
@section('content')
    <?php
            ?>
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">

                        <h4>
                            CODE:{{ $booking->data->cabecera->code }}
                        </h4>
                        <input type="hidden" id="idbooking" value="{{ $booking->data->cabecera->idbooking }}">
                        <small>
                            FECHA:{{ $booking->data->cabecera->fec_ingreso }} al
                            {{ $booking->data->cabecera->fec_salida }}
                        </small>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" id="descuento" class="form-control" placeholder="Descuento%" value="" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <select class="form-control" name="tipo_boleta" id="tipo_boleta">
                                        <option value="" selected disabled>Recibo</option>
                                        <option value="0">Boleta</option>
                                        <option value="1">Factura</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <select class="form-control" name="tipo_pago" id="tipo_pago">
                                        <option value="" selected disabled>Tipo de Pago</option>
                                        <option value="0">Efectivo</option>
                                        <option value="1">Visa</option>
                                        <option value="1">Mastercard</option>
                                    </select>
                                </div><!-- /input-group -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h6>CLIENTE:</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>DNI</th>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>RUC</th>
                                            <th>Email</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                {{ $booking->data->cabecera->dni }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->nombres }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->apellidos }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->ruc }}
                                            </td>
                                            <td>
                                                {{ $booking->data->cabecera->email }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <h6>Detalle</h6>
                                <p>{{ $booking->data->cabecera->detalle }}</p>
                                <h6>Cantidad de Personas</h6>
                                ADULTOS: <span>{{ $booking->data->cabecera->adultos }}</span>
                                NIÑOS: <span>{{ $booking->data->cabecera->ninos }}</span>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h6>HABITACIONES:</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Capacidad</th>
                                            <th>Precio</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($booking->data->detalle as $habitacion)
                                                <tr>
                                                    <td>
                                                        {{$habitacion->nombre}}
                                                    </td>
                                                    <td>
                                                        {{$habitacion->descripcion}}
                                                    </td>
                                                    <td>
                                                        {{$habitacion->capacidad}}
                                                    </td>
                                                    <td>
                                                        {{$habitacion->precio}}
                                                    </td>
                                                </tr>
                                                @empty
                                                No tiene habitaciones asignadas.
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6>PRODUCTOS:</h6>
                                <div class="table-responsible">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($booking->data->productos as $producto)
                                            <tr>
                                                <td>
                                                    {{$producto->nombre}}
                                                </td>
                                                <td>
                                                    {{$producto->descripcion}}
                                                </td>
                                                <td>
                                                    {{$producto->cantidad}}
                                                </td>
                                                <td>
                                                    {{$producto->precio}}
                                                </td>
                                            </tr>
                                        @empty
                                           <span class="text-warning">No tiene productos asignados.</span>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-12 text-right">
                                <button type="button" id="guardar" data-id="" class="btn bg-success"><i class="icon-checkmark position-left"></i>Pagar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        $('#guardar').click(function(){
            var descuento = $('#descuento').val();
            if(descuento === '')descuento = 0;
            var tipo_pago = $('#tipo_pago').val();
            if(tipo_pago === '')descuento = 0;
            var tipo_boleta = $('#tipo_boleta').val();
            if(tipo_boleta === '')descuento = 0;
            var idbooking = $('#idbooking').val();

            var data = {'idbooking':idbooking,'descuento': descuento,"tipo_pago":tipo_pago, "tipo_boleta":tipo_boleta };

            $.post(currentLocation+"pagar",data,function(data){
                window.location.href = currentLocation+"recibo?idbooking="+idbooking;
            });
        });
    </script>
@endsection